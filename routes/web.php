<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
  ]);
Route::group(['middleware' => 'auth'], function () {

    Route::view('/', 'welcome');
    Route::get('/home/get', [App\Http\Controllers\HomeController::class, 'show'])->name('show');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    //task
    Route::view('/task/test', 'pages.task.test');
    Route::get('/task', [App\Http\Controllers\TaskController::class, 'index'])->name('task_list');
    Route::get('/task_get', [App\Http\Controllers\TaskController::class, 'task_show'])->name('task1');
    Route::post('/task/create', [App\Http\Controllers\TaskController::class,'store'])->name('task.create');
    Route::post('/task/update/{id}', [App\Http\Controllers\TaskController::class,'update'])->name('task.update');
    Route::post('/task/archive/{id}', [App\Http\Controllers\TaskController::class,'archive'])->name('task.archive');
    Route::post('/task/rate/{id}', [App\Http\Controllers\TaskController::class,'rate'])->name('task.rate');
    Route::post('/task/drag', [App\Http\Controllers\TaskController::class,'drag'])->name('task.drag');
    Route::get('/task/detail/{id}', [App\Http\Controllers\TaskController::class, 'task_detail'])->name('task.detail');
    Route::view('/task/view/{id}', 'pages.task.components.view');
    Route::delete('/task/delete/{id}', [App\Http\Controllers\TaskController::class, 'destroy'])->name('task.delete');

    
    Route::view('/archive', 'pages.archive.archive');
    Route::get('/archive/getAll', [App\Http\Controllers\TaskController::class, 'archivedTaskSearch'])->name('archivedTaskSearch');
    Route::view('/task2_view', 'pages.task.task2');
    Route::get('/task2', [App\Http\Controllers\TaskController::class, 'task2'])->name('task');
    
    //end task

    Route::post('/comment/create', [App\Http\Controllers\CommentController::class,'create'])->name('comment.create');
    Route::delete('/comment/destroy/{id}', [App\Http\Controllers\CommentController::class,'destroy'])->name('comment.destroy');


    Route::get('/user', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
    Route::get('/user/auth', [App\Http\Controllers\UserController::class, 'getAuth'])->name('users.auth');
    Route::get('/user/getAll', [App\Http\Controllers\UserController::class, 'getAll'])->name('users.getAll');
    Route::post('/user/create', [App\Http\Controllers\UserController::class, 'store'])->name('users.create');
    Route::put('/user/update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');
    Route::put('/user/create', [App\Http\Controllers\UserController::class, 'upAvatar'])->name('users.create');
    Route::delete('/user/delete/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.delete');
    Route::post('/user/password', [App\Http\Controllers\UserController::class, 'password'])->name('users.password');

    Route::get('/department', [App\Http\Controllers\DepartController::class, 'index'])->name('department_show');
    Route::post('/department', [App\Http\Controllers\DepartController::class, 'addDepart'])->name('departments.create');
    Route::put('/department/update', [App\Http\Controllers\DepartController::class, 'update'])->name('departments.update');
    Route::delete('/department/destroy/{id}', [App\Http\Controllers\DepartController::class, 'destroy'])->name('departments.destroy');

    Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile');
    Route::post('/profile/update', [App\Http\Controllers\ProfileController::class, 'update'])->name('profile.update');
    Route::put('/profile/changePassword', [App\Http\Controllers\ProfileController::class, 'store'])->name('profile.changePassword');

    Route::get('/report', [App\Http\Controllers\ReportController::class, 'index'])->name('report');
    Route::post('/report/create', [App\Http\Controllers\ReportController::class, 'store'])->name('report.create');
    Route::get('/report/getAll', [App\Http\Controllers\ReportController::class, 'getAll'])->name('report.getAll');
    Route::delete('/report/delete/{id}', [App\Http\Controllers\ReportController::class, 'destroy'])->name('report.delete');
    Route::post('/report/rate/{id}', [App\Http\Controllers\ReportController::class,'update'])->name('report.rate');



});
