 <!-- Modal to add new user starts-->
 <div class="modal point-report-modal fade" id="point-report-modal">
  <div class="modal-dialog modal-dialog-centered modal-md">
      <form class="point-report-form modal-content pt-0" >
        <meta name="csrf-token" content="{!! csrf_token() !!}">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Тайлан дүгнэх</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <input type="hidden" id="report-id"/>
          <input type="hidden" id="report-point"/>
          <div class="modal-body flex-grow-1">
            <div id="report-detail">
              <div class="font-weight-bold"> Тайлан үүсгэгч </div>
              <div class="media">								
								<div id="creater-profile"></div>
								<!-- upload and reset button -->
								<div class="media-body mt-75 ml-1">
									<p id="creater"></p>
								</div>
								<!--/ upload and reset button -->
              </div>
              <div class="mt-2 font-weight-bold"> Тайлангын нэр </div>
              <div id="report-name" class="mb-2  font-weight-bolder"> </div>
            </div>
						<!-- Rating Stars Box -->
            <div class='rating-stars'>
            <h5 class="mb-50">Үнэлгээ:</h5>
            <ul id='stars'>
              <li class='star' title='Муу' data-value='1'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='Хангалтгүй' data-value='2'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='Дундаж' data-value='3'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='Сайн' data-value='4'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='Маш сайн' data-value='5'>
                <i class='fa fa-star fa-fw'></i>
              </li>
            </ul>
          </div>
          </div>
      </form>
  </div>
</div>
<!-- Modal to add new user Ends-->