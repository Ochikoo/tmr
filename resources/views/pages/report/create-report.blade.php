 <!-- Modal to add new user starts-->
 <div class="modal new-user-modal fade" id="modals-slide-in">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <form class="add-new-user modal-content pt-0" >
          <meta name="csrf-token" content="{!! csrf_token() !!}">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Тайлан оруулах</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body flex-grow-1">
                
                <div class="form-group">
                    <label class="form-label" for="start-date">Эхлэх өдөр</label>
                    <input type="date" id="start-date" name="start_date" class="form-control" placeholder="Эхлэх өдөр" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="end-date">Дуусах өдөр</label>
                    <input type="date" id="end-date" name="end_date" class="form-control" placeholder="Дуусах өдөр" >
                </div>
                <div class="form-group">
                    <label for="attachment">Хавсралт файл</label>
                    <div class="custom-file">
                        <input type="file" name="attachment" class="custom-file-input" id="attachment" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf,.mp4"/>
                        <label class="custom-file-label" for="attachment">файл сонгох</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label" for="viewer">Хянах</label>
                    
                    <select type="text" id="viewer" name="viewer" class="form-control selectpicker" title="Шалгах хүн сонгоогүй байна!"  >
                    <option value="" >сонгох</option>    
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->lastName }} {{ $user->name }}</option>
                        @endforeach
                    </select>     
                </div>

                <button type="submit" class="btn btn-primary mr-1 data-submit">Хадгалах</button>
                <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Болих</button>
            </div>
        </form>
    </div>
  </div>
  <!-- Modal to add new user Ends-->