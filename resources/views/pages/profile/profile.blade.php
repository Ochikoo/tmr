@extends('layouts.wrapper') 


 @section('page-style')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-validation.css') }}">
 @endsection

@section('content')
   <!-- BEGIN: Content-->
   <div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <!-- account setting page -->
    <section id="page-account-settings">
        <div class="row">
            <!-- left menu section -->
            <div class="col-md-3 mb-2 mb-md-0">
                <ul class="nav nav-pills flex-column nav-left">
                    <!-- general -->
                    <li class="nav-item">
                        <a class="nav-link active" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="true">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user font-medium-3 mr-1"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                            <span class="font-weight-bold">Хувийн мэдээлэл</span>
                        </a>
                    </li>
                    <!-- change password -->
                    <li class="nav-item">
                        <a class="nav-link" id="account-pill-password" data-toggle="pill" href="#account-vertical-password" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock font-medium-3 mr-1"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                            <span class="font-weight-bold">Нууц үг солих</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!--/ left menu section -->

            <!-- right content section -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="tab-content">
                            <!-- general tab -->
                            <div role="tabpanel" class="tab-pane active" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">                                
                                <!-- form -->
                                <form class="profile-edit"> 
                                <meta name="csrf-token" content="{{ csrf_token() }}">  
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="user_avatar">Хавсралт файл</label>
                                        <div class="custom-file">
                                          <input type="file" id= "user_avatar" name="avatar" class="custom-file-input"  accept="image/*" />
                                          <label class="custom-file-label text-nowrap text-truncate" for="new-attachment">файл сонгох</label>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="avatar avatar-xl mt-1">
                                            @if($user->avatar!=null)                                        
                                                <img src="avatar/{{$user->avatar}}" id="avatarPreview"alt="avatar">
                                            @else 
                                                <img src="avatar/avatarDefault.png" id="avatarPreview"alt="avatar">
                                            @endif
                                      </div>
                                    </div>
                                  </div>
                                <!--/ header media -->
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="profile_lastName">Овог</label>
                                                <input type="text" class="form-control" id="edit_lastName" name="lastName" placeholder="Username" value="{{$user->lastName }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="profile_name">Нэр</label>
                                                <input type="text" class="form-control" id="edit_name" name="name" placeholder="Name" value="{{$user->name}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="profile_email">Е-мэйл</label>
                                                <input type="email" class="form-control" id="edit_email" name="email" placeholder="Email" value="{{$user->email}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="profile_phone">Утас</label>
                                                <input type="text" class="form-control" id="edit_phone" name="phone" placeholder="Company name" value="{{$user->phone}}">
                                            </div>                                        
                                            <button type="submit" class="btn btn-primary mt-2 mr-1 waves-effect waves-float waves-light" >Хадгалах</button>
                                            <button type="reset" class="btn btn-outline-secondary mt-2 waves-effect">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                                <!--/ form -->
                            </div>
                            <!--/ general tab -->

                            <!-- change password -->
                            <div class="tab-pane fade" id="account-vertical-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">
                                <!-- form -->
                                <form class="password-change"  method="PUT"> 
                                    <meta name="csrf-token" content="{{ csrf_token() }}" />
                                    @csrf
                                        @foreach ($errors->all() as $error)
                                        <p class="text-danger">{{ $error }}</p>
                                        @endforeach 
                                    
                                        <div class="row m-0">
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="old_password">Хуучин нууц үг</label>
                                                    <div class="input-group form-password-toggle input-group-merge">
                                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
                                                        <div class="input-group-append">
                                                            <div class="input-group-text cursor-pointer">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label id="password-error" class="error" for="password" style="display: none;"></label>
                                                </div>                                                                    
                                                <div class="form-group">
                                                    <label for="new_password">Шинэ нууц үг</label>
                                                    <div class="input-group form-password-toggle input-group-merge">
                                                        <input type="password" id="new_password" name="new_password" class="form-control" placeholder="New Password" />
                                                        <div class="input-group-append">
                                                            <div class="input-group-text cursor-pointer">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label id="new_password-error" class="error" for="new_password"></label>
                                                </div>
                                                <div class="form-group">
                                                    <label for="confirm_new_password">Шинэ нууц үгээ давтаж оруулах</label>
                                                    <div class="input-group form-password-toggle input-group-merge">
                                                        <input type="password" class="form-control" id="confirm_new_password" name="confirm_new_password" placeholder="New Password" />
                                                        <div class="input-group-append">
                                                            <div class="input-group-text cursor-pointer"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></div>
                                                        </div>
                                                    </div>
                                                    <label id="confirm_new_password-error" class="error" for="confirm_new_password"></label>
                                                </div>
                                            </div>
                                        </div>
                                            <button type="submit" class="btn btn-primary mr-1 mt-1 waves-effect waves-float waves-light">Хадгалах</button>
                                            <button type="reset" class="btn btn-outline-secondary mt-1 waves-effect">Болих</button>
                                        </div>
                                   
                                </form>
                                <!--/ form -->
                            </div>
                            <!--/ change password -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/ right content section -->
        </div>
    </section>
    <!-- / account setting page -->
</div>
<!-- END: Content-->
@endsection

@section('page-script')

<script>
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $("#avatarPreview").attr("src", e.target.result);
            };

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $("#user_avatar").change(function() {
        readURL(this);
    });

    $(function () {
        $(".profile-edit").validate({
            errorClass: "error",
            rules: {
                lastName: {
                required: true
            },
                name: {
                required: true
            },
            email: {
                required: true
            },
            phone: {
                maxlength: 8,
                    minlength: 8,
                    number: true
            },
            }
        });    
        
        $(".profile-edit").on("submit", function(e) {
        e.preventDefault();
        var id =$(this)[0].id;
        var isValid = $(".profile-edit").valid();
        if(isValid) {
        var lastName = $("#edit_lastName").val();
        var name = $("#edit_name").val();
        var email = $("#edit_email").val();
        var phone = $("#edit_phone").val();
        var avatar = $("#user_avatar[type='file']");
        var data = new FormData();
        $.each(avatar[0].files, function() {
        data.append("avatar", this);
        });
        data.append(
                    "_token",
                    $('meta[name="csrf-token"]').attr("content")
                );
        data.append("name",name);
        data.append("lastname", lastName);
        data.append("email", email);
        data.append("phone", phone);
            if(confirm(" Хувийн мэдээлэл өөрчлөх үү")) {
                $.ajax({
                    url: "/profile/update",
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response) {
                        if (response) {
                           
                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });
                            
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            }
        };
    });
});

$(function () {
        $(".password-change").validate({
            errorClass: "error",
            rules: {
                password: {
                    required: true,
                },
                new_password: {
                    required: true,
                    minlength: 8
                },
                "confirm_new_password": {
                    minlength: 8,
                    equalTo: "#new_password"
                }
            }
        });    
    $(".password-change").on("submit", function(e) {
    e.preventDefault();
    var id =$(this)[0].id;
   
   var isValid = $(".password-change").valid();
    if(isValid){
    var password = $("#password").val();
    var new_password = $("#new_password").val();
    var new_confirm_password = $("#confirm_new_password").val();
        if(confirm("Нууц үг өөрчлөх үү")) {
		$.ajax({
			url: "/profile/changePassword/" + id,
            type: "PUT",
            data:{
                password: password,
                new_password: new_password,
                new_confirm_password: new_confirm_password,
            },
			headers : {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
            },
			success: function(response) {
				if(response) {
					toastr["success"](response.success, "Амжилттай", {
						closeButton: true,
						tapToDismiss: false
					});
    			location.reload();
                }
			},
			error: function(response) {
                var msg = response.statusText;
                var res = JSON.parse(response.responseText)
                if(response.responseText && res.errors && res.errors.password) {
                    msg =  res.errors.password[0];
                }
				toastr["warning"](msg, "Алдаа", {
					closeButton: true,
					tapToDismiss: false
				});
			}
        });
        }   
     }  
     }); 
    });
</script>
   

     <script src="{{  asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
     <script src="{{  asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js')  }}"></script>
     

     <!-- END: Page Vendor JS-->
      <!-- BEGIN: Page JS-->
      {{-- <script src="{{  asset('app-assets/js/scripts/pages/app-user-list.js')  }}"></script> --}}
      <script src="{{  asset('app-assets/js/scripts/pages/page-account-settings.js') }}"></script>

      <!-- END: Page JS-->
    <!-- END: Page JS-->   
@endsection