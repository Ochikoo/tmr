 <!-- Modal to edit user starts-->
 <div class="modal password-user-modal fade" id="modals-password-user">
  <div class="modal-dialog modal-dialog-centered modal-md">
      <form class="password-user-form modal-content pt-0" >
            <meta name="csrf-token" content="{!! csrf_token() !!}">        
            <input type="hidden" id="user-pwd-id" name="id" aria-describedby="basic-icon-default-fullname2" />
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Хэрэглэгчийн нууц үг солих</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body flex-grow-1">
              <div class="mb-2 d-flex">
                <div id="user-profile"> 
                </div>
                <div id="user-name" class="pt-3 ml-1"> 
                </div>
              </div>                                                                     
              <div class="form-group">
                <label for="password">Шинэ нууц үг</label>
                <div class="input-group form-password-toggle input-group-merge">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Шинэ нууц үг" />
                    <div class="input-group-append">
                        <div class="input-group-text cursor-pointer">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                        </div>
                    </div>
                </div>
                <label id="password-error" class="error" for="password"></label>
            </div>
            <div class="form-group">
                <label for="confirm_password">Шинэ нууц үгээ давтаж оруулах</label>
                <div class="input-group form-password-toggle input-group-merge">
                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Шинэ нууц үг баталгаажуулах" />
                    <div class="input-group-append">
                        <div class="input-group-text cursor-pointer"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></div>
                    </div>
                </div>
                <label id="confirm_password-error" class="error" for="confirm_password"></label>
            </div>
              <button type="submit" class="btn btn-primary mr-1 data-submit">Хадгалах</button>
              <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Болих</button>
          </div>
      </form>
  </div>
</div>
<!-- Modal to edit user Ends-->