 <!-- Modal to edit user starts-->
 <div class="modal edit-user-modal fade" id="modals-edit-user">
  <div class="modal-dialog modal-dialog-centered modal-md">
      <form class="edit-user-form modal-content pt-0" >
            <meta name="csrf-token" content="{!! csrf_token() !!}">        
            <input type="hidden" id="user-edit-id" name="id" aria-describedby="basic-icon-default-fullname2" />
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Хэрэглэгчийн мэдээлэл засах</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body flex-grow-1">
              <div class="form-group">
                  <label class="form-label" for="user-edit-name">Нэр</label>
                  <input type="text" class="form-control dt-full-name" id="user-edit-name" name="name" aria-describedby="basic-icon-default-fullname2" />
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-edit-lastname">Овог</label>
                  <input type="text" id="user-edit-lastname" class="form-control dt-uname"  aria-describedby="user-edit-uname2" name="lastName" />
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-edit-email">Е-мейл</label>
                  <input type="text" id="user-edit-email" class="form-control dt-email" placeholder="john.doe@example.com" aria-label="john.doe@example.com" aria-describedby="user-edit-email2" name="email" />
                  <small class="form-text text-muted">Е-мейл оруулахдаа үсэг, тоо, цэг ашиглаж болно </small>
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-edit-department-id">Хэлтэс</label>
                  <select id="user-edit-department-id" name="department_id" class="form-control">
                    <option value="">-select-</option>
                    @foreach ($departs as $depart)
                    <option value="{{ $depart->id }}">{{ $depart->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-edit-role">Үүрэг</label>
                  <select id="user-edit-role" name="role" class="form-control">
                    <option value="">-select-</option>
                      <option value="MANAGER">Дарга</option>
                      <option value="STAFF">Ажилчин</option>
                  </select>
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-edit-phone">Утас</label>
                  <input type="text" id="user-edit-phone" class="form-control dt-uname" name="phone" />
              </div>                                                                                                                                                        
              <div class="form-group">
                <label class="form-label" for="user-edit-token">Рокет чат токен</label>
                <input type="text" class="form-control dt-full-name" id="user-edit-token" name="token" aria-describedby="user-fullname2" />
              </div>                                 
              <div class="form-group">
                <label class="form-label" for="user-edit-position">Албан тушаал</label>
                <input type="text" class="form-control dt-full-name" id="user-edit-position" name="position" aria-describedby="user-edit-fullname2" />
              </div>
              <button type="submit" class="btn btn-primary mr-1 data-submit">Хадгалах</button>
              <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Болих</button>
          </div>
      </form>
  </div>
</div>
<!-- Modal to edit user Ends-->