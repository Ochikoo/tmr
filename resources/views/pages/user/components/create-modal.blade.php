 <!-- Modal to add new user starts-->
 <div class="modal new-user-modal fade" id="modals-slide-in">
  <div class="modal-dialog modal-dialog-centered modal-md">
      <form class="add-new-user modal-content pt-0" >
        <meta name="csrf-token" content="{!! csrf_token() !!}">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Шинэ хэрэглэгч</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body flex-grow-1">
              <div class="form-group">
                  <label class="form-label" for="user-name">Нэр</label>
                  <input type="text" class="form-control dt-full-name" id="user-name" name="name" aria-describedby="basic-icon-default-fullname2" />
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-lastname">Овог</label>
                  <input type="text" id="user-lastname" class="form-control dt-uname"  aria-describedby="user-uname2" name="lastName" />
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-email">Е-мейл</label>
                  <input type="text" id="user-email" class="form-control dt-email" placeholder="john.doe@example.com" aria-label="john.doe@example.com" aria-describedby="user-email2" name="email" />
                  <small class="form-text text-muted">Е-мейл оруулахдаа үсэг, тоо, цэг ашиглаж болно </small>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="user-avatar">Хавсралт файл</label>
                    <div class="custom-file">
                      <input type="file" id= "user-avatar" name="avatar" class="custom-file-input"  accept="image/*" />
                      <label class="custom-file-label text-nowrap text-truncate" for="new-attachment">файл сонгох</label>
                    </div>
                </div>
                </div>
                <div class="col-md-4">
                  <div class="avatar avatar-xl mt-1">
                    <img src="avatar/avatarDefault.png" id="avatarPreview"alt="avatar">
                  </div>
                </div>
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-department-id">Хэлтэс</label>
                  <select id="user-department-id" name="department_id" class="form-control">
                    <option value="">-select-</option>
                    @foreach ($departs as $depart)
                    <option value="{{ $depart->id }}">{{ $depart->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-role">Үүрэг</label>
                  <select id="user-role" name="role" class="form-control">
                    <option value="">-select-</option>
                      <option value="MANAGER">Дарга</option>
                      <option value="STAFF">Ажилчин</option>
                  </select>
              </div>
              <div class="form-group">
                  <label class="form-label" for="user-phone">Утас</label>
                  <input type="text" id="user-phone" class="form-control dt-uname" name="phone" />
              </div>                                    
              <div class="form-group">
                <label class="form-label" for="user-token">Рокет чат токен</label>
                <input type="text" class="form-control dt-full-name" id="user-token" name="token" aria-describedby="user-fullname2" />
              </div>                                  
              <div class="form-group">
                <label class="form-label" for="user-position">Албан тушаал</label>
                <input type="text" class="form-control dt-full-name" id="user-position" name="position" aria-describedby="user-fullname2" />
              </div>                                
              <div class="form-group">
                <label class="form-label" for="user-password">Нууц үг</label>
                <input type="password" autocomplete="new-password" class="form-control dt-full-name" id="user-password" name="password" aria-describedby="user-fullname2" />
              </div>                             
              <div class="form-group">
                <label class="form-label" for="user-password-confirmation">Нууц үг дахин оруулах</label>
                <input type="password" autocomplete="new-password" class="form-control dt-full-name" id="user-password-confirmation" name="password-confirmation" aria-describedby="basic-icon-default-fullname2" />
              </div>
              <div class="form-group">
                  <b><span class="text-success" id="success-message"> </span><b>
              </div>
              <button type="submit" class="btn btn-primary mr-1 data-submit">Хадгалах</button>
              <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Болих</button>
          </div>
      </form>
  </div>
</div>
<!-- Modal to add new user Ends-->