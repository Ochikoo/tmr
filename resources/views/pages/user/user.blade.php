@extends('layouts.wrapper') 


 @section('page-style')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css')}} ">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-validation.css') }}">
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    <!-- END: Custom CSS-->
 @endsection

@section('content')
   <!-- BEGIN: Content-->
   <div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users list start -->
            <section class="app-user-list">
                <!-- list section start -->
                <div class="card">
                    <div class="card-datatable table-responsive pt-0">
                        <table class="user-list-table table">
                            <thead class="thead-light">
                                <tr>
                                    <th>Хэрэглэгч</th>
                                    <th>Е-мейл</th>
                                    <th>Утас</th>
                                    <th>Албан тушаал</th>
                                    <th>Үүрэг</th>
                                    <th>Хэлтэс</th>
                                    <th>Үйлдлүүд</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- Modal to add new user starts-->
                    @include('pages.user.components.create-modal')
                    <!-- Modal to add new user Ends-->
                    <!-- Modal to edit user starts-->
                    @include('pages.user.components.edit-modal')
                    @include('pages.user.components.password-modal')
                    <!-- Modal to edit user Ends-->
                </div>
                <!-- list section end -->
            </section>
            <!-- users list ends -->

        </div>
    </div>
</div>
<!-- END: Content-->
@endsection

@section('page-script')
   <!-- BEGIN: Page JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js')  }}"></script>
     <!-- END: Page Vendor JS-->
      <!-- BEGIN: Page JS-->
      <script src="{{  asset('app-assets/js/scripts/pages/app-user-list.js')  }}"></script>
      <!-- END: Page JS-->
    <!-- END: Page JS-->   
@endsection