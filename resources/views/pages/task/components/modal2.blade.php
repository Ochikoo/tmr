
<!-- Kanban Sidebar starts -->
<div class="modal new-item-sidebar fade" id="task-grid-modal2">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content p-0">
      <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Second</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
      </div>
          <div class="modal-body flex-grow-1">
          <form class="modal-1-form" enctype="multipart/form-data">
              Second modal content
          </form>
          </div>
      </div>
  </div>
</div>
<!-- Kanban Sidebar ends -->
