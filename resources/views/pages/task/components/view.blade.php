@extends('layouts.wrapper')
@section('page-style')
  <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/css/plugins/forms/form-validation.css')}}">
    <!-- END: Page CSS-->

 @endsection
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content ">
	<div class="content-wrapper">
	    <div class="content-body">
        <div > 
					<div > 
						<div class="card ">
							<div class="card-body">
								<ul class="nav nav-pills">
									<li class="nav-item">
										<a class="nav-link active" id="home-tab" data-toggle="pill" href="#home" aria-expanded="true">Дэлгэрэнгүй</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="profile-tab" data-toggle="pill" href="#profile" aria-expanded="false">Үйл явц</a>
									</li>
								</ul>
								<div class="tab-content border p-2 detail-item-sidebar">
									<div role="tabpanel" class="tab-pane active " id="home" aria-labelledby="home-tab" aria-expanded="true">
										<div class="media">								
											<div id="creater-profile">
											</div>
											<!-- upload and reset button -->
											<div class="media-body mt-75 ml-1">
												<p id="owner"></p>
												<p id="owner_position"></p>
											</div>
											<!--/ upload and reset button -->
										</div>
										<!--/ header media -->
										<div class="card-body px-0">
													
											<div class="row m-0 mt-2">
												<div class="col-md-6 p-0"> 
													<h5 class="mb-50">Гарчиг:</h5>
													<p class="card-text" id="title"></p>
												</div>
												<div class="col-md-6 p-0"> 										
													<div class="col-md-6 p-0"> 
														<!-- Rating Stars Box -->
														<div class=''>
															<h5 class="mb-50">Төлөв:</h5>
															<div class="btn-group btn-group-toggle btn-group-sm" id="btn-group-status" data-toggle="buttons">
																<label class="btn btn-outline-danger waves-effect"  id="label_new" >
																	<input type="radio" name="status-radio" id="radio_new" value="NEW">
																	Шинэ
																</label>
																<label class="btn btn-outline-warning waves-effect"  id="label_proccesing">
																	<input type="radio" name="status-radio" id="radio_proccesing"  value="IN_PROGRESS">
																	Хийгдэж буй
																</label>
																<label class="btn btn-outline-success waves-effect" id="label_awaiting">
																	<input type="radio" name="status-radio" id="radio_awaiting" value="AWAIT">
																	Дууссан
																</label>
														</div>
														</div>
													</div>
												</div>
											</div>		
											<div class="row m-0 mt-2">
												<div class="col-md-6 p-0"> 
													<h5 class="mb-50">Гүйцэтгэгч:</h5>
													<div class="avatar-group" id="avatar-group"></div>
												</div>
												<div class="col-md-6 p-0"> 
													<!-- Rating Stars Box -->
												 <div class='rating-stars'>
													 <h5 class="mb-50">Үнэлгээ:</h5>
													 <ul id='stars'>
														 <li class='star' title='Муу' data-value='1'>
															 <i class='fa fa-star fa-fw'></i>
														 </li>
														 <li class='star' title='Хангалтгүй' data-value='2'>
															 <i class='fa fa-star fa-fw'></i>
														 </li>
														 <li class='star' title='Дундаж' data-value='3'>
															 <i class='fa fa-star fa-fw'></i>
														 </li>
														 <li class='star' title='Сайн' data-value='4'>
															 <i class='fa fa-star fa-fw'></i>
														 </li>
														 <li class='star' title='Маш сайн' data-value='5'>
															 <i class='fa fa-star fa-fw'></i>
														 </li>
													 </ul>
												 </div>
												</div>
											</div>
											<div class="row m-0 mt-2">
												<div class="col-md-6 p-0"> 
													<h5 class="mb-75">Эхлэсэн өдөр:</h5>
													<p class="card-text" id="created_at"></p>
												</div>
												<div class="col-md-6 p-0"> 
													<h5 class="mb-75">Дуусах өдөр:</h5>
													<p class="card-text" id="end_date"></p>
												</div>
											</div>
											<div class="mt-2">
												<h5 class="mb-50">Хавсралт файл:</h5>
												<div class="row m-0" id="file-group"> 
												</div>
											</div>								
											<div class="mt-2">
												<h5 class="mb-75">Тайлбар:</h5>
												<p class="card-text" id="description"></p>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab" aria-expanded="false">				
																		
										<div class="row m-0"> 
											<div class="col-12 col-sm-8"> 
												<div id="comments" class="overflow-hidden"></div>
													<div class="modal-body flex-grow-1">
														<form class="comment-form" id="send_comment" >
														<meta name="csrf-token" content="{!! csrf_token() !!}">
															<div class="form-group">
																	<label for="comment-attachment">Хавсралт файл</label>
																	<div class="custom-file">
																			<input 
																				type="file"
																				name="attachment[]"
																				class="custom-file-input"
																				id="comment-attachment"
																				accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf,.mp4"
																				multiple
																			/>
																			<label class="custom-file-label" for="comment-attachment">файл сонгох</label>
																	</div>
															</div>
															<div class="form-group">
																	<label class="form-label" for="item-description">Тайлбар</label>
																	<input type="text" id="this_id" name="task_id" hidden/>
																	<textarea  rows="4" type="text" id="detail-item-comment" name="comment" class="form-control" placeholder="Enter Description" ></textarea>
															</div>                             
															<div class="form-group">
																	<div class="d-flex flex-wrap">
																			<button class="btn btn-primary mr-1 data-submit waves-effect waves-float waves-light" type="submit">Илгээх</button>
																			<button type="button" class="btn btn-outline-secondary waves-effect" data-dismiss="modal">Болих</button>
																	</div>
															</div>
														</form>												
													</div>
												</div>       
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@section('page-script')
<script src="{{ asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('app-assets/js/scripts/pages/app-task.js')}}"></script> 
@endsection