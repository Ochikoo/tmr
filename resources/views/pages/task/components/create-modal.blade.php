
<!-- Kanban Sidebar starts -->
<div class="modal new-item-sidebar fade">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-0">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Таск үүсгэх</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
            <div class="modal-body flex-grow-1">
            <form class="new-item-form" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="form-label" for="item-title">Гарчиг</label>
                    <input type="text" id="item-title" name="title" class="form-control" placeholder="Гарчиг оруулах" />
                </div>
                
                <meta name="csrf-token" content="{!! csrf_token() !!}">
                <div class="form-group">
                    <label class="form-label" for="item-priority">Зэрэглэл</label>
                    <select type="text" id="item-priority" name="priority" class="form-control" title="Зэрэглэл сонгох" >
                        <option value="LOW">Энгийн</option>
                        <option value="NORMAL">Дундаж</option>
                        <option value="HIGH">Яаралтай</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="form-label" for="item-assign_id">Гүйцэтгэгч</label>
                    <select type="text" id="item-assign_id" name="assign_id" class="form-control selectpicker" title="Гүйцэтгэгч сонгогдоогүй байна" multiple >
                        @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->lastName }} {{ $user->name }}</option>
                        @endforeach
                    </select>                                         
                </div>
                <div class="form-group">
                    <label class="form-label" for="new-end-date">Дуусах өдөр</label>
                    <input type="date" id="new-end-date" name="end_date" class="form-control" placeholder="Дуусах өдөр" >
                </div>
                <div class="form-group">
                    <label for="new-attachment">Хавсралт файл</label>
                    <div class="custom-file">
                        <input type="file" name="attachment[]" class="custom-file-input" id="new-attachment" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf,.mp4" multiple/>
                        <label class="custom-file-label" for="new-attachment">файл сонгох</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label" for="item-description">Тайлбар</label>
                    <textarea  rows="4" type="text" id="item-description" name="description" class="form-control" placeholder="Enter Description" ></textarea>
                </div>                                    
                <div class="form-group">
                    <div class="d-flex flex-wrap">
                        <button class="btn btn-primary mr-1 data-submit waves-effect waves-float waves-light" type="submit">Хадгалах</button>
                        <button type="button" class="btn btn-outline-secondary waves-effect" data-dismiss="modal">Болих</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Kanban Sidebar ends -->
