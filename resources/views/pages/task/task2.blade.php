@extends('layouts.wrapper') 
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content ">
	<div class="content-wrapper">
		<div class="content-body">
				<!-- list section start -->
      <div class="card">
        <div class="card-datatable table-responsive">
          
          <table class="archive-list-table table">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th>Таскын нэр</th>
                <th>Зэрэглэл</th>
                <th>Үүсгэгч</th>
                <th>Гүйцэтгэгчид</th>
                <th>Дэлгэнгүй / Засах</th>
                <th>Дуусгах Огноо</th>
              </tr>
            </thead> 
          </table>          
        </div>        
    </div>
    
  {{-- Pagination --}}
  {{-- <div class="d-flex justify-content-center">
    {{$tasks->withQueryString()->links()}}
  </div> --}}
		<!-- list section end -->
		<!-- users list ends -->
	</div>
</div>
</div>
<!-- END: Content-->
@endsection

@section('page-script')


@section('page-script')
   <!-- BEGIN: Page JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js')  }}"></script>
     <script src="{{  asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js')  }}"></script>
     <!-- END: Page Vendor JS-->
      <!-- BEGIN: Page JS-->
      <script src="{{  asset('app-assets/js/scripts/pages/app-task-list.js')  }}"></script>
      <!-- END: Page JS-->
    <!-- END: Page JS-->   
@endsection