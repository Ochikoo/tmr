@extends('layouts.wrapper') 


 @section('page-style')
  <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/vendors/css/jkanban/jkanban.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/vendors/css/editors/quill/katex.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/vendors/css/editors/quill/monokai-sublime.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/vendors/css/editors/quill/quill.snow.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/vendors/css/editors/quill/quill.bubble.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/css/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/css/plugins/forms/form-quill-editor.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/css/plugins/forms/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('app-assets/css/pages/app-kanban.css')}}">    
    <!-- END: Page CSS-->

 @endsection

@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content kanban-application">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Kanban starts -->
                <section class="app-kanban-wrapper">                
                    <!-- Kanban content starts -->                    
                   @if(Auth::user()->role == "ADMIN" || Auth::user()->role == "MANAGER")
                    <button type="button" class="btn btn-outline-primary waves-effect creat-task-button">                       
                        <span>Таск үүсгэх</span>
                    </button>
                    @endif
                    <div class="kanban-wrapper"></div>
                    <!-- Kanban content ends -->
                    <!-- Kanban Sidebar starts -->
                   @if(Auth::user()->role == "ADMIN" || Auth::user()->role == "MANAGER")
                    @include('pages.task.components.create-modal')
                    @include('pages.task.components.edit-modal')
                   @endif
                    @include('pages.task.components.detail-modal')
                    <!-- Kanban Sidebar ends -->
                </section>
                <!-- Kanban ends -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection

@section('page-script')

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/jkanban/jkanban.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/editors/quill/katex.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/editors/quill/highlight.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/editors/quill/quill.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
        <script src="{{ asset('app-assets/js/scripts/pages/app-kanban.js')}}"></script>
    <!-- END: Page JS-->

@endsection