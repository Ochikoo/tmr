@extends('layouts.wrapper')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content ">
	<div class="content-overlay"></div>
	<div class="header-navbar-shadow"></div>
	<div class="content-wrapper">
		<div class="content-header row"> </div>
		<div class="content-body">
			<!-- users list start -->
			<section class="app-user-list"> 
				@if(Auth::user()->role == "ADMIN")
				@include('pages.department.create-depart')
				@endif
				<!-- list section start -->
				<div class="card">
					<div class="card-datatable table-responsive pt-0">
						<table class="user-list-table table">
							<thead class="thead-light">
								<tr>
									<th>#</th>
									<th>Хэлтсийн нэр</th>
									<th>Утас</th>
									<th>Хаяг</th>
									@if(Auth::user()->role == "ADMIN")
									<th>Үйлдлүүд</th>
									@endif
								</tr>
							</thead> @foreach ($data as $index => $da)
							<tbody>
								<tr>
									<td>{{$index + 1 }}</td>
									<td>{{$da->name }}</td>
									<td>{{$da->phone }}</td>
									<td>{{$da->address }}</td>
									@if(Auth::user()->role == "ADMIN")
										<td>
											<div class="d-flex">
												<button
														type="button"
														data-toggle="modal" 
														data-target="#modals-edit-user{{$da->id }}"
														class="btn btn-icon rounded-circle btn-outline-primary waves-effect edit"
												>
														<svg
																xmlns="http://www.w3.org/2000/svg"
																width="14"
																height="14"
																viewBox="0 0 24 24"
																fill="none"
																stroke="currentColor"
																stroke-width="2"
																stroke-linecap="round"
																stroke-linejoin="round"
																class="feather feather-edit-2"
														>
																<path
																		d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"
																></path>
														</svg>
														</button>
													
														<button
															type="submit"
															class="btn btn-icon rounded-circle btn-outline-primary waves-effect delete-depart ml-1"
															id="{{$da->id}}"
															>
															<svg
																	xmlns="http://www.w3.org/2000/svg"
																	width="14"
																	height="14"
																	viewBox="0 0 24 24"
																	fill="none"
																	stroke="currentColor"
																	stroke-width="2"
																	stroke-linecap="round"
																	stroke-linejoin="round"
																	class="feather feather-trash-2"
															>
																	<polyline points="3 6 5 6 21 6"></polyline>
																	<path
																			d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"
																	></path>
																	<line x1="10" y1="11" x2="10" y2="17"></line>
																	<line x1="14" y1="11" x2="14" y2="17"></line>
															</svg>
													</button>
										</div>				
										@include('pages.department.edit-depart')					
									</td>
									@endif
					</div>
					</tr>
					</tbody> @endforeach </table>
				</div>
		</div>
		<!-- list section end -->
		</section>
		<!-- users list ends -->
	</div>
</div>
</div>
@endsection 
@section('page-script')
<!-- BEGIN: Page JS-->
<script src="{{ asset('app-assets/js/scripts/components/components-modals.js')}}"></script>
<script>
$(".delete-depart").on("click", function(e) {
	e.preventDefault();

	var departId =$(this)[0].id;
	console.log(departId);
	if(confirm("Хэлтэсийг устгахдаа итгэлтэй байна уу ? Хэлтэстэй хамааралтай хэрэглэгч болон бүх мэдээлэл устах болно !")) {
		$.ajax({
			url: "/department/destroy/" + departId,
			type: "DELETE",
			headers: {
				"X-CSRF-TOKEN": "{{ csrf_token() }}",
			},
			success: function(response) {
				if(response) {
					toastr["success"](response.success, "Амжилттай", {
						closeButton: true,
						tapToDismiss: false
					});
    			location.reload();
				}
			},
			error: function(response) {
				toastr["warning"](response.statusText, "Алдаа", {
					closeButton: true,
					tapToDismiss: false
				});
			}
		});
	}
});
</script> 
@endsection