 <!-- Modal to edit user starts-->
 <div class="modal edit-user-modal fade" id="modals-edit-user{{$da->id }}">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <form class="edit-user-form modal-content pt-0" action="{{ route ('departments.update')}}" method="POST"> {{ method_field('PUT')}} @csrf
            <meta>
            <input type="hidden" id="user-edit-id" name="id" value="{{$da->id }}" aria-describedby="basic-icon-default-fullname2" />
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Хэлтсийн мэдээлэл засах</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
            <div class="modal-body flex-grow-1">
                <div class="form-group">
                    <label class="form-label" for="user-name">Хэлтсийн нэр</label>
                    <input type="text" class="form-control dt-full-name" id="depart-name" value="{{$da->name }}" name="name" aria-describedby="basic-icon-default-fullname2" /> </div>
                <div class="form-group">
                    <label class="form-label" for="user-lastname">Утас</label>
                    <input type="text" id="depart-phone" class="form-control dt-phone" value="{{$da->phone }}" name="phone" /> </div>
                <div class="form-group">
                    <label class="form-label" for="user-email">Хаяг</label>
                    <input type="text" id="depart-address" class="form-control dt-address" aria-label="john.doe@example.com" aria-describedby="user-email2" value="{{$da->address }}" name="address" /> </div>
                <button type="submit" class="btn btn-primary mr-1 data-submit">Хадгалах</button>
                <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Болих</button>
            </div>
        </form>
    </div>
</div>
  <!-- Modal to edit user Ends-->