 <!-- Disabled Backdrop -->
 <div class="disabled-backdrop-ex">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary mb-2" data-toggle="modal" data-backdrop="false" data-target="#backdrop">
        Хэлтэс нэмэх
    </button>
    <!-- Modal -->
    <div class="modal fade text-left" id="backdrop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="add-department modal-content pt-0" action="{{route('departments.create')}}" method="POST" >
                    @csrf
                    <meta>
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Шинэ хэлтэс</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                          </button>
                      </div>
                      <div class="modal-body flex-grow-1">
                          <div class="form-group">
                              <label class="form-label" for="user-name">Хэлтэс нэр</label>
                              <input type="text" class="form-control dt-full-name" id="depart-name" name="name" aria-describedby="basic-icon-default-fullname2" />
                          </div>
                          <div class="form-group">
                              <label class="form-label" for="user-lastname">Утас</label>
                              <input type="text" id="depart-phone" class="form-control dt-phone"  name="phone" />
                          </div>
                          <div class="form-group">
                              <label class="form-label" for="user-email">Хаяг</label>
                              <input type="text" id="depart-address" class="form-control dt-address"  aria-label="john.doe@example.com" aria-describedby="user-email2" name="address" />
                      
                          </div>
                          <div class="form-group">
                              <b><span class="text-success" id="success-message"> </span><b>
                          </div>
                          <button type="submit" class="btn btn-primary mr-1 data-submit">Хадгалах</button>
                          <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Болих</button>
                      </div>
                  </form>
            </div>
        </div>
    </div>
</div>
<!-- Disabled Backdrop end-->

