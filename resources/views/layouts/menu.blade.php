<!-- BEGIN: Main Menu-->
<div
    class="main-menu menu-fixed menu-light menu-accordion menu-shadow"
    data-scroll-to-active="true"
>
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a
                    class="navbar-brand"
                    href="/"
                >
                <div class="avatar">
                        <img src="{{ asset('ardchilsn.png')}}" alt="avatar" width="32" height="32" />
                    </div>
                    <h2 class="brand-text">Ардчилсан</h2>
                </a>
            </li>
            <li class="nav-item nav-toggle">
                <a
                    class="nav-link modern-nav-toggle pr-0"
                    data-toggle="collapse"
                    ><i
                        class="d-block d-xl-none text-primary toggle-icon font-medium-4"
                        data-feather="x"
                    ></i
                    ><i
                        class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary"
                        data-feather="disc"
                        data-ticon="disc"
                    ></i
                ></a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul
            class="navigation navigation-main"
            id="main-menu-navigation"
            data-menu="menu-navigation"
        >
            <li class=" navigation-header">
                <span data-i18n="Apps &amp; Pages">Цэс</span
                ><i data-feather="more-horizontal"></i>
            </li>
            <li class=" nav-item" id="user-menu">
                <a class="d-flex align-items-center" href="/user"
                    ><i data-feather="users"></i
                    ><span class="menu-title text-truncate" data-i18n="Todo"
                        >Хэрэглэгч</span
                    ></a
                >
            </li>
            <li class=" nav-item" id="department-menu">
                <a class="d-flex align-items-center" href="/department"
                    ><i data-feather="layers"></i
                    ><span class="menu-title text-truncate" data-i18n="Todo"
                        >Хэлтэс</span
                    ></a
                >
            </li>
            <li class=" nav-item" id="task-menu">
                <a class="d-flex align-items-center" href="/task"
                    ><i data-feather="check-square"></i
                    ><span class="menu-title text-truncate" data-i18n="Todo"
                        >Таск</span
                    ></a
                >
            </li>
            <li class=" nav-item" id="archive-menu">
                <a class="d-flex align-items-center" href="/archive"
                    ><i data-feather="archive"></i
                    ><span class="menu-title text-truncate" data-i18n="Todo"
                        >Архив</span
                    ></a
                >
            </li>
            <li class=" nav-item" id="archive-menu">
                <a class="d-flex align-items-center" href="/report"
                    ><i data-feather="archive"></i
                    ><span class="menu-title text-truncate" data-i18n="Todo"
                        >Тайлан</span
                    ></a
                >
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
