<!-- BEGIN: Header-->
<nav
    class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow"
>
    <div class="navbar-container d-flex content">
    <div class="bookmark-wrapper d-flex align-items-center">
        <ul class="nav navbar-nav d-xl-none">
            <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i data-feather="menu"></i></a></li>
        </ul>
    </div>
        <ul class="nav navbar-nav align-items-center ml-auto">  
            <li class="nav-item">
                <a class="nav-link nav-link-style" data-prev-layout=""
                    ><svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        class="ficon"
                    >
                        <path
                            d="M21 12.79A9 9 0 1 1 11.21 3 7 7 0 0 0 21 12.79z"
                        ></path>
                    </svg>
                </a>
            </li>
       
            @if (Auth::guest())
                @if (Route::has('login'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown dropdown-user">
                    <a
                        class="nav-link dropdown-toggle dropdown-user-link"
                        id="dropdown-user"
                        href="javascript:void(0);"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        <div class="user-nav d-sm-flex d-none">
                            <span class="user-name font-weight-bolder">
                                {{ Auth::user()->name }}
                            </span>
                            <span class="user-status">
                                {{ Auth::user()->email }}
                            </span>
                        </div>
                        <span class="avatar"
                            >
                            @if (Auth::user()->avatar!=null)
                            <img
                                class="round"
                                src="/avatar/{{Auth::user()->avatar}}"
                                alt="avatar"
                                height="40"
                                width="40"/>
                            @else
                            <div class="avatar bg-light-secondary">
                                <span class="avatar-content" style="font-size: 20px">{{substr(Auth::user()->name, 0, 2)}}</span>
                            </div>
                            @endif
                            <span
                                class="avatar-status-online"
                            ></span
                        ></span>
                    </a>
                    <div
                        class="dropdown-menu dropdown-menu-right"
                        aria-labelledby="dropdown-user"
                    >
                        <a class="dropdown-item" href="/profile" >
                            <i class="mr-50" data-feather="user"></i> 
                            Мэдээлэл
                        </a>
                        <a class="dropdown-item" href="/task">
                            <i class="mr-50" data-feather="check-square"></i>
                            Даалгавар
                        </a>
                        <div class="dropdown-divider"></div>
                        <form id="logout-form" method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a class="dropdown-item" href="#" onclick="document.getElementById('logout-form').submit()">
                                <i class="mr-50" data-feather="power"></i>
                                Гарах
                            </a>
                        </form>
                    </div>
                </li>
            @endif
            
        </ul>
    </div>
</nav>
<!-- END: Header-->
