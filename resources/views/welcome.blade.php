@extends('layouts.wrapper') 
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Ecommerce Starts -->
                <section id="dashboard-ecommerce">
                    <div class="row match-height">
                        <!-- Medal Card -->
                        <div class="col-xl-4 col-md-6 col-12">
                        <div class="card earnings-card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-6">
                                                    <h4 class="card-title mb-1">Таск</h4>
                                                    <div class="font-small-2">Энэ сар танд</div>
                                                    <h5 class="mb-1" id="total_task"></h5>
                                                    <p class="card-text text-muted font-small-2">
                                                        <span class="font-weight-bolder" id="finished"></span><span> дууссагсан байна.</span>
                                                    </p>
                                                </div>
                                                <div class="col-6">
                                                    <div id="earnings-chart"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                        <!--/ Medal Card -->
                        <!-- Statistics Card -->
                        <div class="col-xl-8 col-md-6 col-12">
                            <div class="card card-statistics">
                                <div class="card-header">
                                    <h4 class="card-title">Статистик</h4>
                                    <div class="d-flex align-items-center">
                                    </div>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                            <div class="media">
                                                <div class="avatar bg-light-primary mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="trending-up" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0" id="progress"></h4>
                                                    <p class="card-text font-small-3 mb-0">Хийгдэж буй</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                            <div class="media">
                                                <div class="avatar bg-light-info mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0" id="finished_2"></h4>
                                                    <p class="card-text font-small-3 mb-0">Дуусан</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                            <div class="media">
                                                <div class="avatar bg-light-danger mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="box" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0" id="completed"></h4>
                                                    <p class="card-text font-small-3 mb-0">Дүгнэсэн</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-6 col-12">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="dollar-sign" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0" id="timeOut"></h4>
                                                    <p class="card-text font-small-3 mb-0">Хугацаа хэтэрсэн</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Statistics Card -->
                    </div>
                    @if(Auth::user()->role=="MANAGER")
                    <div class="row match-height" id="isManager">
                        <!-- Avg Sessions Chart Card starts -->
                        <div class="col-lg-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <h4 class="card-title">Хэлтэсийн таск</h4>
                                    <!-- <i data-feather="help-circle" class="font-medium-3 text-muted cursor-pointer"></i> -->
                                </div>
                                <div class="card-body p-0">
                                    <div id="goal-overview-radial-bar-chart" class="my-2"></div>
                                    <div class="row border-top text-center mx-0">
                                        <div class="col-3 border-right py-1">
                                            <p class="card-text text-muted mb-0">Хийгдэж буй</p>
                                            <h3 class="font-weight-bolder mb-0" id="depart-progress"></h3>
                                        </div>
                                        <div class="col-3 border-right py-1">
                                            <p class="card-text text-muted mb-0">Дуусан</p>
                                            <h3 class="font-weight-bolder mb-0" id="depart-await"></h3>
                                        </div>
                                        <div class="col-3 border-right py-1">
                                            <p class="card-text text-muted mb-0">Дүгнэсэн</p>
                                            <h3 class="font-weight-bolder mb-0" id="depart-completed"></h3>
                                        </div>
                                        <div class="col-3  py-1">
                                            <p class="card-text text-muted mb-0">Хугацаа хэтэрсэн</p>
                                            <h3 class="font-weight-bolder mb-0" id="depart-timeout"></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Avg Sessions Chart Card ends -->

                        <!-- Support Tracker Chart Card starts -->
                        <div class="col-lg-6 col-12">
                  
                        </div>
                        <!-- Support Tracker Chart Card ends -->
                    </div>
                    @endif

                    <div class="row match-height">
                   
                 
                    </div>
                </section>
                <!-- Dashboard Ecommerce ends -->

            </div>
        </div>
    </div>
    
    <!-- END: Content-->
    
@endsection
@section('page-script')
    <script src="{{ asset ('app-assets/js/scripts/pages/home-statistic.js')}}"></script>

    <script src="{{ asset ('app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
@endsection