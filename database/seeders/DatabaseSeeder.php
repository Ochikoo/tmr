<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
            DB::table('departments')->insert([
                'name' => "Depart 1",
                'address' => "Depart 1 hayag",
                'phone' => '92839283',
            ]);
            DB::table('departments')->insert([
                'name' => "Depart 2",
                'address' => "Depart 2 hayag",
                'phone' => '92839283',
            ]);
            $arrayValues = ['ADMIN', 'STAFF', 'MANAGER'];
            foreach (range(1,5) as $index) {
                DB::table('users')->insert([
                    'name' => "хэрэглэгч -" . $index,
                    'lastName' => "овог -" . $index,
                    'role' => $arrayValues[rand(0,2)],
                    'email' => 'user'.$index.'@gmail.com',
                    'phone' => Str::random(8),
                    'department_id'=> rand(1,2),
                    'password' => Hash::make('password'),
                ]);
        }
        DB::table('users')->insert([
            'name' => "Admin",
            'lastName' => "admin",
            'role' => "ADMIN",
            'email' => 'adminsuper@gmail.com',
            'phone' => "99110000",
            'department_id'=> rand(1,2),
            'password' => Hash::make('WaxaxaQ!W@E#'),
        ]);
    }
}
