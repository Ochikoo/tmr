<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {           
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('description');
            $table->enum('status', ['NEW', 'IN_PROGRESS', 'AWAIT']);
            $table->timestamp('end_date');
            $table->integer('points');
            $table->enum('priority', ['LOW','NORMAL','HIGH', 'HIGHEST']);
            $table->bigInteger('user_id')->unsigned();
            $table->boolean('archived')->default(false);
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
