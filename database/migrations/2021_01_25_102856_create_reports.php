<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('path');
            $table->string('name');
            $table->timestamp('end_date')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->integer('points');
            $table->bigInteger('creater_id')->unsigned();
            $table->foreign('creater_id')->references('id')->on('users');
            $table->bigInteger('viewer_id')->unsigned();
            $table->foreign('viewer_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
