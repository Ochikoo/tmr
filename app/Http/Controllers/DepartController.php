<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\User;
use App\Models\User_Task;
use App\Models\Comment;
use App\Models\CommentAttach;
use App\Models\Attachment;
use App\Models\Task;
use DataTables;
use File;

class DepartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data= DB::table('departments')->get();
        
        return view('pages.department.department')-> with ('data', $data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addDepart(Request $request)
    {
        //
        Department::create([
            'name'=> $request->name,
            'phone'=> $request->phone,
            'address'=> $request->address,
        ]);
        // User::create($request->all());
            return redirect("department");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
   
        $depart = Department::findOrFail($request->id);
        $input = $request->all();
        

        $depart->fill($input)->save();
        return redirect("department");
        // return response()->json([ 'success'=> 'Хэрэглэгч амжилттай нэмэгдлээ!']);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user =  Auth::user();
        if($user->role == 'ADMIN'){
            $users = User::where("department_id", "=", $id)->get();
            foreach($users as $u){
                $tasks = Task::where("user_id", "=", $u->id)->get();
                $assings = User_Task::where("user_id", "=", $u->id)->get();
                foreach($tasks as $task){             
                    User_Task::where("task_id","=", $task->id)->delete();
                    
                    $comments = Comment::where("task_id","=",  $task->id)->get();
                    foreach($comments as $comment) {
                        $commAttach = CommentAttach::where("comment_id","=", $comment->id)->get();            
                        foreach($commAttach as $attach) {
                            File::delete('attachment/'.$attach->path);
                            $attach->delete();
                        }
                    }
                    Comment::where("task_id","=", $task->id)->delete();
                    
                    $attachments = Attachment::where("task_id","=", $task->id)->get();            
                    foreach($attachments as $attach) {
                        File::delete('attachment/'.$attach->path);
                        $attach->delete();
                    }
                }

                foreach($assings as $a){             
                    User_Task::where("task_id","=", $a->task_id)->delete();
                    $comments = Comment::where("task_id","=", $a->task_id)->get();
                    foreach($comments as $comment) {
                        $commAttach = CommentAttach::where("comment_id","=", $comment->id)->get();            
                        foreach($commAttach as $attach) {
                            File::delete('attachment/'.$attach->path);
                            $attach->delete();
                        }
                    }
                    Comment::where("task_id","=", $a->task_id)->delete();
                    
                    $attachments = Attachment::where("task_id","=", $a->task_id)->get();            
                    foreach($attachments as $attach) {
                        File::delete('attachment/'.$attach->path);
                        $attach->delete();
                    }
                    Task::destroy($a->task_id);
                }                
                User_Task::where("user_id","=", $u->id)->delete();
                Comment::where("user_id","=", $u->id)->delete();
                Task::where("user_id","=", $u->id)->delete();
            }
            User::where("department_id", "=", $id)->delete();
            Department::destroy($id);
            return response()->json([ 'success'=> 'Хэлтэс амжилттай устгагдлаа']);
        }
    }
    
}
