<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Task;
use App\Models\Attachment;
use App\Models\User_Task;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        return view('welcome');
    }

    public function show()
    {
        $currenUser=user::find(Auth::id());
        
        $status= Task::leftjoin('task_user', 'tasks.id', '=', 'task_user.task_id')
        ->select(array('status', DB::raw('count(status) as count')))
        ->where("archived", "=", false)
        ->where("task_user.user_id", "=", Auth::id())
        ->whereMonth("tasks.end_date", "=", Carbon::now()->month)
        ->groupBy('tasks.status')
        ->get();

        $rows['total']=0;
        $rows['completed']=0;
        $rows['await']=0;
        foreach($status as $row){
            $rows['total']+=$row->count;
            if($row->status=="AWAIT")
                $rows['await']=$row->count;
        }
        
        $rows['completed']= Task::join('task_user', 'tasks.id', '=', 'task_user.task_id')
        ->whereMonth("tasks.end_date", "=", Carbon::now()->month)
        ->where("status", "==", "COMPLETED")
        ->where("task_user.user_id", "=", Auth::id())
        ->count();

        //saraar haih ->whereMonth("end_date", "=", Carbon::now()->month)
        $rows['timeOut']= Task::join('task_user', 'tasks.id', '=', 'task_user.task_id')
        ->where("archived", "=", false)
        ->where("end_date", "<", Carbon::today())
        ->where("status", "!=", "COMPLETED")
        ->where("status", "!=", "AWAIT")
        ->where("task_user.user_id", "=", Auth::id())
        ->count();


        $rows['progress']=$rows['total']-($rows['completed']+$rows['await']);
        $rows['finished']=$rows['completed']+$rows['await'];

        
        $rows["isManager"]=false;
        if($currenUser->role=="MANAGER"){
            $rows["isManager"]=true;

            $depart_count= Task::leftjoin('task_user', 'tasks.id', '=', 'task_user.task_id')
            ->leftjoin('users', 'users.id', '=', 'task_user.task_id')
            ->select(array('status', DB::raw('count(status) as count')))
            ->where("archived", "=", false)
            ->whereMonth("end_date", "=", Carbon::now()->month)
            ->where("users.department_id", "=", $currenUser->department_id)
            ->distinct()
            ->groupBy('tasks.status')
            ->get();

            $rows['depart']['completed']=0;
            $rows['depart']['new']=0;
            $rows['depart']['in_progress']=0;
            $rows['depart']['await']=0;
            $rows['depart']['total']=0;
            foreach($depart_count as $row){
                $rows['depart']['total']+=$row->count;
                if($row->status=="COMPLETED")
                    $rows['depart']['completed']=$row->count;
                elseif($row->status=="NEW")
                    $rows['depart']['new']=$row->count;
                elseif($row->status=="IN_PROGRESS")
                    $rows['depart']['in_progress']=$row->count;
                elseif($row->status=="AWAIT")
                    $rows['depart']['await']=$row->count;

            }

            $rows['depart']['timeOut']= Task::join('task_user', 'tasks.id', '=', 'task_user.task_id')
            ->leftjoin('users', 'users.id', '=', 'task_user.task_id')
            ->where("archived", "=", false)
            ->where("end_date", "<", Carbon::today())
            ->where("status", "!=", "COMPLETED")
            ->where("status", "!=", "AWAIT")
            ->where("users.department_id", "=", $currenUser->department_id)
            ->distinct()
            ->count();

            }
        


        return $rows;
    }
}
