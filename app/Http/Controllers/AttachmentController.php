<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\Task;
use App\Models\Attachment;
use File;

use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    //
    public function destroy($id)
    {        
            // Attachment::destroy($id);
            $attach = Attachment::findOrFail($id);
            File::delete('attachment/'.$attach->path);
            $attach->delete();
            return response()->json([ 'success'=> 'Файл амжилттай устгагдлаа!']);
        //
    }
}
