<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Task;
use App\Models\Attachment;
use App\Models\Comment;
use App\Models\CommentAttach;
use App\Models\User_Task;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Str;
use Carbon\Carbon;
use File;
use DataTables;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {   $users = User::select('name', 'id', 'lastName')->get();
        return view('pages.task.task')->with("users", $users);
        //return view('pages.department.depart');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function task_show()
    {   
        $rows= Task::with(["comments","assigns", "attachments"])
        ->where("archived", "=", false)
        ->get();

        foreach ($rows as $row){
            $tmp=json_encode($row);
            

            $comments=DB::table('task_user')
            ->where('task_id', $row->id)
            ->get();        
            
            $to_json=$tmp.json_encode($comments);

        }
        
        $to_json="";   
        $idss = array();
        foreach ($rows as $row){
            $tmp=json_encode($row);
            

            $comments=DB::table('task_user')
            ->where('task_id', $row->id)
            ->get();        
            
            $to_json=$tmp.json_encode($comments);

        }
        
        $rows= json_encode($rows);
        
        $rows=json_decode($rows);
        

        $NEW=array();
        $IN_PROGRESS=array();
        $TESTING=array();
        $AWAIT=array();
        $COMPLETED=array();

        foreach($rows as $row){
            $tmp_c=0;
            foreach($row->assigns as $assign){
                if( $assign->pivot->user_id ==Auth::id()){
                    $tmp_c+=1;
                }
            }

            if($row->user_id == Auth::id() || $tmp_c!=0){

                $row->assigns = json_encode($row->assigns);
                $row->comments = count($row->comments);
                $row->attachments = json_encode($row->attachments);
            
                if($row->status =="NEW"){
                    array_push($NEW, $row);
                }
                elseif($row->status =="IN_PROGRESS" || $row->status =="TESTING"){
                    array_push($IN_PROGRESS, $row);
                }

                elseif($row->status =="AWAIT"){
                    array_push($AWAIT, $row);
                }
            }
        }

        $json=[["id"=> "board-in-NEW","status"=>"NEW","title"=> "Шинэ",'item'=>$NEW],
            ["id"=> "board-in-IN_PROGRESS","status"=>"IN_PROGRESS", "title"=> "Хийгдэж буй",'item'=>$IN_PROGRESS],
            ["id"=> "board-in-AWAIT","status"=>"AWAIT","title"=> "Дууссан",'item'=>$AWAIT],
        ];
        
        
        return $json;
        //return view('pages.department.depart');
    }

    public function archivedTaskSearch(Request $request){
        
        $tasks= Task::with(["assigns"])
        ->leftJoin('users', 'tasks.user_id', '=', 'users.id')
        ->select("tasks.*", "users.name", "users.lastname", "users.position","users.avatar")
        ->where("archived", "=", true)
        ->where("title", "LIKE" , "%".$request->searchTerm."%")
        ->orderBy('updated_at', 'asc')
        ->get();
        // ->get();
        foreach($tasks as$index => $task) {
            
            if($task->user_id!=Auth::id()){
                $assigned = null;
                foreach($task->assigns  as $assign) {

                    if($assign->pivot->user_id == Auth::id()){
                        $assigned = $assign;
                        break;
                    }
                }
                
                if($assigned == null ) {
                        
                    unset($tasks[$index]);
                    
                }
            }
        }
        // ->paginate(15);
        // return $tasks;
        
        $userData = User::with('department');
        return Datatables::of($tasks)->make(true);
        // return view('pages.archive.archive', ["tasks" => $tasks]);
    }

    public function task2(Request $request){
        
        $tasks= Task::with(["assigns"])
        ->leftJoin('users', 'tasks.user_id', '=', 'users.id')
        ->select("tasks.*", "users.name", "users.lastname", "users.position","users.avatar")
        ->where("archived", "=", false)
        ->where("title", "LIKE" , "%".$request->searchTerm."%")
        ->orderBy('updated_at', 'asc')
        ->get();

        foreach($tasks as$index => $task) {
            
            if($task->user_id!=Auth::id()){
                $assigned = null;
                foreach($task->assigns  as $assign) {

                    if($assign->pivot->user_id == Auth::id()){
                        $assigned = $assign;
                        break;
                    }
                }
                
                if($assigned == null ) {
                        
                    unset($tasks[$index]);
                    
                }
            }
        }
        
        $userData = User::with('department');
        return Datatables::of($tasks)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(Carbon::parse($request->end_date));
        $dateTime = Carbon::parse($request->end_date);
        $request['end_date'] = $dateTime->format('Y-m-d H:i:s');


        $task = Task::create([
            'title'=> $request->title,
            'description'=> $request->description,
            'status'=> 'NEW',
            'points'=>0,
            'priority'=> $request->priority,
            'end_date'=>$request->end_date ,
            'user_id'=> Auth::id(),
        ]);

        if ($image = $request->file('attachment')) {
            foreach ($image as $files) {
            $destinationPath = 'attachment/'; // upload path
            $name = $files->getClientOriginalName();
            $profile = Str::orderedUuid() . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profile);
            Attachment::create([
                'path'=>"$profile",
                'task_id'=>$task->id,
                'name'=>$name
            ]);
            }
        }
        $color="#00D8FF";
        if($request->priority=="HIGH"){
            $color="#FF0000";
        }
        elseif($request->priority=="NORMAL"){
            $color="#FF9B00";
        }
        foreach(json_decode($request->assign_id) as $assign){
            User_Task::create([
                'user_id'=> $assign,
                'seen'=>false,
                'task_id'=>$task->id
            ]);

            $assign_user=User::findOrFail($assign);
            if(isset($assign_user->token)){
                $tmp=['text' => 'Таск нэмэгдлээ' , 'attachments'=>[['title'=>$request->title, 'title_link'=>"http://work.democratic.mn/task/view/".$task->id, "color"=>$color,'text' =>$request->description."  \nДуусгах өдөр: ".$request->end_date]]];

                $response = Curl::to('https://chat.democratic.mn/hooks/'.$assign_user->token)
                ->withData($tmp) 
                ->asJson()
                ->post();
            }
            

        }
        $user=User::select( "token")
            ->findOrFail($task->user_id);
            if(isset($user->token)){
                    
                $tmp=['text' => 'Таск нэмэгдлээ' , 'attachments'=>[['title'=>$request->title, 'title_link'=>"http://work.democratic.mn/task/view/".$task->id, "color"=>$color,'text' =>$request->description."  \nДуусгах өдөр: ".$request->end_date]]];

                $response = Curl::to('https://chat.democratic.mn/hooks/'.$user->token)
                ->withData($tmp) 
                ->asJson()
                ->post();

            }

       

        

        return response()->json([ 'success'=> 'Таск амжилттай нэмэгдлээ!']);
    }

    public function getStatusName($status){
         $myCar = [
             "NEW"=> "Шинэ",
             "IN_PROGRESS"=> "Хийгдэж буй",
             "TESTING" =>"Шалгаж буй",
             "AWAIT" => "Дууссан",
             "COMPLETED" =>"Дүгнэсэн"
         ];
         return $myCar[$status];
    }

    //get data task detail
    public function task_detail(Request $request)
    {
       
        $task = Task::where('tasks.id', '=', $request->id)
        ->leftJoin('users', 'tasks.user_id', '=', 'users.id')
        ->select("tasks.*", "users.name", "users.lastname", "users.position","users.avatar")
        ->with(["comments","assigns","owner", "attachments"])
        ->get();
        return $task;
    }

    // task status update
    public function drag(Request $request)
    {
        //
        $task = Task::findOrFail($request->id);
        $assigns = User_Task::where("task_id","=", $task->id)->get();
        $assigned = null;
        foreach($assigns as $assign) {
            if($assign->user_id == Auth::id()){
                $assigned = $assign;
                break;
            }
        }
        if($assigned != null) {
            $task->status = $request->status;
            $task->save();

            $color="#00D8FF";
            if($task->priority=="HIGH"){
                $color="#FF0000";
            }
            elseif($task->priority=="NORMAL"){
                $color="#FF9B00";
            }

            $status='';
            if($task->status =="NEW"){
                $status="шинэ";
            }
            elseif($task->status =="IN_PROGRESS"){
                $status="хийгдэж буй";
            }
            elseif($task->status =="AWAIT"){
                $status="дууссан";
            }
            
            $task_users=User_task::select( "users.token")
            ->leftjoin('users', 'task_user.user_id', '=', 'users.id')
            ->where("task_user.task_id", "=", $task->id)
            ->get();

            foreach($task_users as $user){
                if(isset($user->token)){
                    
                $tmp=['text' => 'Таскын өөрчлөлт _*статус: '.$status.'*_' , 'attachments'=>[['title'=>$task->title, 'title_link'=>"http://work.democratic.mn/task/view/".$task->id, "color"=>$color,'text' =>$task->description."  \nДуусгах өдөр: ".$task->end_date]]];

                $response = Curl::to('https://chat.democratic.mn/hooks/'.$user->token)
                ->withData($tmp) 
                ->asJson()
                ->post();

                }
            }

            $user=User::select( "token")
            ->findOrFail($task->user_id);
            if(isset($user->token)){
                    
                $tmp=['text' => 'Таскын өөрчлөлт _*статус: '.$status.'*_' , 'attachments'=>[['title'=>$task->title, 'title_link'=>"http://work.democratic.mn/task/view/".$task->id, "color"=>$color,'text' =>$task->description."  \nДуусгах өдөр: ".$task->end_date]]];

                $response = Curl::to('https://chat.democratic.mn/hooks/'.$user->token)
                ->withData($tmp) 
                ->asJson()
                ->post();

            }


        return response()->json([ 'success'=> '<p>Таскын төлөв <b>'.$this->getStatusName($request->status).'</b>-рүү шилжлээ</p>']);
        }else{
            $returnData = array(
                'status' => 'Алдаа',
                'statusText' => 'Та энэ үйлдэлийг хийх эрхгүй байна !'
            );
            return response()->json($returnData, 500);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $task = Task::where('tasks.id', '=', $request->id)
        ->leftJoin('users', 'tasks.user_id', '=', 'users.id')
        ->select("tasks.*", "users.name", "users.lastname", "users.position","users.avatar")
        ->with(["comments","assigns","owner", "attachments"])
        ->get();


        return $task;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       User_Task::where("task_id","=", $id)->delete();
        foreach(json_decode($request->assign_id) as $assign){
            User_Task::create([
                'user_id'=> $assign,
                'seen'=>true,
                'task_id'=>$id
            ]);
        }
        $task = Task::findOrFail($id);
        $task->title = $request->title;
        $task->description = $request->description;
        $task->priority = $request->priority;
        $task->end_date = $request->end_date;
        $task->save();
        
        if ($image = $request->file('attachment')) {
           foreach ($image as $files) {
           $destinationPath = 'attachment/'; // upload path
           $profile = Str::orderedUuid() . "." . $files->getClientOriginalExtension();
           $name = $files->getClientOriginalName();
           $files->move($destinationPath, $profile);
           Attachment::create([
               'path'=>"$profile",
               'task_id'=>$task->id,
               'name'=>$name
           ]);
           }
       }
        
        return response()->json([ 'success'=> 'Таскын мэдээлэл амжилттай өөрчлөгдлөө']);
    }

    public function rate(Request $request, $id)
    {
        //
        $user = Auth::user();
        $task = Task::findOrFail($id);
        if($task->status == 'AWAIT' && $user->role=='ADMIN'|| $user->role=='MANAGER') {
            $task->points = $request->rate;
            $task->archived = true;
            $task->save();
            return response()->json([ 'success'=> 'Таскын гүйцэтгэлийг үнэлсэнд баярлалаа']);
        }
    }

    public function archive($id)
    {
        //
        $task = Task::findOrFail($id);
        if($task->points > 0 && $task->status == 'AWAIT'){
            $task->archived = true;
            $task->save();        
            return response()->json([ 'success'=> 'Таск архив руу шилжсэн']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $task =  Task::findOrFail($id);
        $user = Auth::user();
        if($task->user_id == $user->id){
            User_Task::where("task_id","=", $id)->delete();
            $comments = Comment::where("task_id","=", $id)->get();
            foreach($comments as $comment) {
                $commAttach = CommentAttach::where("comment_id","=", $comment->id)->get();            
                foreach($commAttach as $attach) {
                    File::delete('attachment/'.$attach->path);
                    $attach->delete();
                }

            }
           Comment::where("task_id","=", $id)->delete();
            $attachments = Attachment::where("task_id","=", $id)->get();            
            foreach($attachments as $attach) {
                File::delete('attachment/'.$attach->path);
                $attach->delete();
            }
            $task->delete();
            return response()->json([ 'success'=> 'Таск амжилттай устгагдлаа']);
        }
    }
}
