<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DataTables;
use App\Models\User;
use App\Models\Report;
use App\Models\Department;
use File;

class reportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::select("name", "id", "lastName")
        ->where("role", "=", "MANAGER")
        ->get();
        return view('pages.report.report')->with('users', $users);
    }

    
    public function getAll(Request $request){
        if ($request->ajax()) {
            $userId = Auth::id();
            $reports =  Report::with(["creater", "viewer"])
            ->where('creater_id', '=', $userId)
            ->orWhere('viewer_id',"=", $userId);
            return Datatables::of($reports)
                ->make(true);
        }
      }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    
        $profile = null;
        $name=null;
        if($request->hasFile('attachment')){
                $image = $request->file('attachment');
                $destinationPath = 'reports/'; // upload path
                $profile = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $name = $image->getClientOriginalName();
                $image->move($destinationPath, $profile);
        }
            Report::create([
                'start_date'=> $request->start_date,
                'end_date'=> $request->end_date,
                'viewer_id'=> $request->viewer_id,
                'points'=>'0',
                'name'=>$name,
                'path'=>$profile,
                'creater_id'=>Auth::id(),
                
            ]);
            // User::create($request->all());
            

            return response()->json([ 'success'=> 'Хэрэглэгч амжилттай нэмэгдлээ!']);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
            //
        $userId = Auth::id();
        $report = Report::findOrFail($id);
        if($report->viewer_id == $userId) {
            $report->points = $request->rate;
            $report->save();
            return response()->json([ 'success'=> 'Тайлангын гүйцэтгэлийг үнэлсэнд баярлалаа']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $report  = Report::findOrFail($id);
        $userId = Auth::id();
        if($report->creater_id == $userId){
            File::delete('reports/'.$report->path);
            $report->delete();

            return response()->json([ 'success'=> 'Тайлан амжилттай устгагдлаа!']);
        }
    }
}
