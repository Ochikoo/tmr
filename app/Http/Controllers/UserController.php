<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Department;
use App\Models\User_Task;
use App\Models\Comment;
use App\Models\CommentAttach;
use App\Models\Attachment;
use App\Models\Task;
use DataTables;
use Illuminate\Support\Facades\Storage;
use File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $departs = Department::get();
        return view('pages.user.user')->with('departs', $departs);
    }

     /*
   AJAX request
   */
   public function getAll(Request $request){
    if ($request->ajax()) {
        $userData = User::with('department');
        return Datatables::of($userData)
            ->make(true);
    }
  }

  public function getAuth(){
    $user=Auth::user();
    unset($user["email"]);
    unset($user["token"]);
    unset($user["phone"]);
    return $user;
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(Auth::user()->role == "ADMIN") {
            $profile = null;
            if($request->hasFile('avatar')){
                    $image = $request->file('avatar');
                    $destinationPath = 'avatar/'; // upload path
                    $profile = date('YmdHis') . "." . $image->getClientOriginalExtension();
                    $image->move($destinationPath, $profile);
            }
                User::create([
                    'name'=> $request->name,
                    'lastName'=> $request->lastName,
                    'email'=> $request->email,
                    'avatar'=> $profile,
                    'role'=> $request->role,
                    'token'=>$request->rtoken,
                    'department_id'=>$request->department_id,
                    'phone'=> $request->phone,
                    'position'=> $request->position,
                    'password' => Hash::make($request->password),
                ]);
                // User::create($request->all());
                

                return response()->json([ 'success'=> 'Хэрэглэгч амжилттай нэмэгдлээ!']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = Auth::user();
        if($user->role == 'ADMIN'){
            $user = User::findOrFail($id);

            $input = $request->all();

            $user->fill($input)->save();

            return response()->json([ 'success'=> 'Хэрэглэгчийн мэдээлэл амжилттай өөрчлөгдлөө']);
        }
        
    }

    public function password(Request $request)
    {   
        //
        if(Auth::user()->role == "ADMIN"){
            $user = User::findOrFail($request->id);
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json([ 'success'=> 'Хэрэглэгчийн нууц үг амжилттай өөрчлөгдлөө']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $user = Auth::user();
        $duser  = User::findOrFail($id);
        if($user->role == 'ADMIN' && $duser->role !="ADMIN") {

            $tasks = Task::where("user_id", "=", $id)->get();
            $assings = User_Task::where("user_id", "=", $id)->get();
            foreach($tasks as $task){             
                User_Task::where("task_id","=", $task->id)->delete();
                $comments = Comment::where("task_id","=", $task->id)->get();
                foreach($comments as $comment) {
                    $commAttach = CommentAttach::where("comment_id","=", $comment->id)->get();            
                    foreach($commAttach as $attach) {
                        File::delete('attachment/'.$attach->path);
                        $attach->delete();
                    }
                }
                Comment::where("task_id","=", $task->id)->delete();
                $attachments = Attachment::where("task_id","=", $task->id)->get();            
                foreach($attachments as $attach) {
                    File::delete('attachment/'.$attach->path);
                    $attach->delete();
                }
            }
            foreach($assings as $a){             
                User_Task::where("task_id","=", $a->task_id)->where("user_id","=", $id)->delete();
                $comments = Comment::where("task_id","=", $a->task_id)->get();
                foreach($comments as $comment) {
                    $commAttach = CommentAttach::where("comment_id","=", $comment->id)->get();            
                    foreach($commAttach as $attach) {
                        File::delete('attachment/'.$attach->path);
                        $attach->delete();
                    }
                }
                Comment::where("task_id","=", $a->task_id)->delete();
                $attachments = Attachment::where("task_id","=", $a->task_id)->get();            
                    foreach($attachments as $attach) {
                        File::delete('attachment/'.$attach->path);
                        $attach->delete();
                    }
            }                
            Comment::where("user_id","=", $id)->delete();
            Task::where("user_id","=", $id)->delete();
            if($duser->avatar !=null) {
                File::delete('avatar/'.$duser->avatar);
            }
            $duser->delete();
            return response()->json([ 'success'=> 'Хэрэглэгчийг амжилттай устгалаа!']);
        }
        return response()->json([ 'success'=> 'Хэрэглэгчийг устгах боломжгүй байна!']);
    }
}
