<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User_Task;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Task;
use App\Models\Comment;
use App\Models\CommentAttach;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Str;
use File;
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $comment = Comment::create([
            'text'=> $request->text,
            'task_id'=>$request->task_id,
            'user_id'=> Auth::id(),
        ]);

        if ($image = $request->file('attachment')) {
            foreach ($image as $files) {
            $destinationPath = 'attachment/'; // upload path
            $name = $files->getClientOriginalName();
            $profile = Str::orderedUuid() . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profile);
            CommentAttach::create([
                'path'=>"$profile",
                'comment_id'=>$comment->id,
                'name'=>$name
            ]);
            }
        }

        $task= Task::find($request->task_id);
        
        $task_users=User_task::select( "users.token")
        ->leftjoin('users', 'task_user.user_id', '=', 'users.id')
        ->where("task_user.task_id", "=", $request->task_id)
        ->get();

        foreach($task_users as $user){
            if(isset($user->token)){
            $tmp=['text' => 'Сэтгэгдэл бичигдлээ', 'attachments'=>[['title'=>$task->title, 'title_link'=>"http://work.democratic.mn/task/view/".$task->id, 'text' =>'Шинэ сэтэгдэл: '.$request->text]]];

            $response = Curl::to('https://chat.democratic.mn/hooks/'.$user->token)
            ->withData($tmp) 
            ->asJson()
            ->post();
            }
        }
        $user=User::select( "token")
        ->findOrFail($task->user_id);
        if(isset($user->token)){
                
           $tmp=['text' => 'Сэтгэгдэл бичигдлээ', 'attachments'=>[['title'=>$task->title, 'title_link'=>"http://work.democratic.mn/task/view/".$task->id, 'text' =>'Шинэ сэтэгдэл: '.$request->text]]];

            $response = Curl::to('https://chat.democratic.mn/hooks/'.$user->token)
            ->withData($tmp) 
            ->asJson()
            ->post();
            

        }


        return response()->json([ 'success'=> 'Коммент амжилттай илгээгдлээ!'.$task->title]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $user = Auth::user();
        if($comment->user_id == $user->id || $user->role == "ADMIN" || $user->role=="MANAGER") {
            $attachments = CommentAttach::where("comment_id","=", $id)->get();            
            foreach($attachments as $attach) {
                File::delete('attachment/'.$attach->path);
                $attach->delete();
            }
            Comment::destroy($id);
            return response()->json([ 'success'=> 'Коммент амжилттай устгагдлаа!']);
        }
        //
    }
}
