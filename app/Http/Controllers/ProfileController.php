<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Department;
use App\Models\User_Task;
use App\Models\Comment;
use App\Models\Attachment;
use App\Models\Task;
use DataTables;
use Illuminate\Support\Facades\Storage;
use File;

class profileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $user = User::findOrFail(Auth::id());
        return view('pages.profile.profile')->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'confirm_new_password' => ['same:new_password'],
        ]);
        $user=User::find(Auth::id());
        $user->password= Hash::make($request->new_password);
        $user->save();
        
        return response()->json([ 'success'=> 'Нууц үг амжилттай солигдлоо']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $user = User::findOrFail(Auth::id());
        $user->name = $request->name;
        $user->lastName = $request->lastname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if($request->hasFile('avatar')){
            File::delete('avatar/'.$user->avatar);
            $image = $request->file('avatar');
             $destinationPath = 'avatar/'; // upload path
             $profile = date('YmdHis') . "." . $image->getClientOriginalExtension();
             $image->move($destinationPath, $profile);
             $user->avatar = "$profile";
        }
        $user->save();
      
        return response()->json([ 'success'=> ' Хувийн мэдээлэл амжилттай өөрчлөгдлөө']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
