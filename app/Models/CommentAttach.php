<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentAttach extends Model
{
    use HasFactory;
    protected $fillable = [
        'path',
        'comment_id',
        'name',
    ];

    protected $table = "files";
}
