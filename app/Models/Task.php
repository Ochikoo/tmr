<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User_Task;
use App\Models\Comment;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'status',
        'points',
        'priority',
        'user_id',
        'group_id',
        'end_date',
        'assigns'
    ];

    public function userTasks(){
        return $this->hasMany(User_Task::class);
    }

    public function assigns(){
        return $this->belongsToMany(User::class)->select("name","lastName", "position","avatar", "email");
    }

    public function owner(){
        return $this->hasOne(User::class, "id", "user_id")->select("name","lastName", "position","avatar", "email");
    }

    public function comments(){
        return $this->hasMany(Comment::class)->with("attachments")
        ->leftJoin('users', 'comments.user_id', '=', 'users.id')
        ->select("users.name", "users.lastname", "users.position", "comments.*")
        ->orderBy('created_at', 'asc');
    }

    public function attachments(){
        return $this->hasMany(Attachment::class)->orderBy('created_at', 'asc');
    }


}
