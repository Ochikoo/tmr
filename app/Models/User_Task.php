<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Task;

class User_Task extends Model
{
    use HasFactory;
    
    
    protected $fillable = [
        'user_id',
        'task_id',
        'seen',
    ];

    
    protected $table = "task_user";

    protected function tasks(){
        return $this->belongsTo(Task::class);
    }
}
