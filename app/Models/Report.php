<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Report extends Model{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'path',
        'viewer_id',
        'creater_id',
        'start_date',
        'end_date',
        'points',
    ]; 
    
    public function creater()
    {
        return $this->belongsTo(User::class, 'creater_id');
    }

    public function viewer()
    {
        return $this->belongsTo(User::class, 'viewer_id');
    }

}
