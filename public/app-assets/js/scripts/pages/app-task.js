$(function() {
    "use strict";

    var authUser, detail_data;

    // Get User
    function ajax2() {
        return $.ajax({
            type: "GET",
            dataType: "json",
            async: false,
            url: "/user/auth",
            success: function(data) {
                authUser = data;
            }
        });
    }

    $.when(ajax2());

    // Render avatar
    function renderAvatar(users, pullUp, margin, size) {
        var $transition = pullUp ? " pull-up" : "";
        return users !== undefined
            ? users
                  .map(function(user, index, arr) {
                      var $margin =
                          margin !== undefined && index !== arr.length - 1
                              ? " mr-" + margin + ""
                              : "";
                      var stateNum = Math.floor(Math.random() * 6) + 1;
                      var states = [
                          "success",
                          "danger",
                          "warning",
                          "info",
                          "dark",
                          "primary",
                          "secondary"
                      ];
                      var colorClass = " bg-light-" + states[stateNum] + " ";
                      if (user.avatar === undefined || user.avatar === null) {
                          return (
                              "<li class='avatar kanban-item-avatar" +
                              " " +
                              $transition +
                              " " +
                              $margin +
                              "'" +
                              "data-toggle='tooltip' data-placement='top'" +
                              "title='" +
                              user.name +
                              "'" +
                              ">" +
                              "<div class='avatar-container'><div class='avatar" +
                              colorClass +
                              "' style='margin-right: 3px' title='" +
                              user.lastName +
                              " " +
                              user.name +
                              "' >" +
                              "<span class='avatar-content' style='font-size: 20px'>" +
                              user.name.substring(0, 1) +
                              "</span></div></div></li>"
                          );
                      }
                      return (
                          "<li class='avatar kanban-item-avatar" +
                          " " +
                          $transition +
                          " " +
                          $margin +
                          "'" +
                          "data-toggle='tooltip' data-placement='top'" +
                          "title='" +
                          user.name +
                          "'" +
                          ">" +
                          "<img src='" +
                          "/avatar/" +
                          user.avatar +
                          "' alt='Avatar' height='" +
                          size +
                          "' width='" +
                          size +
                          "'>" +
                          "</li>"
                      );
                  })
                  .join(" ")
            : "";
    }

    function renderAttach(attachments) {
        var tmpattach = "";
        attachments.map(function(file, index, arr) {
            tmpattach +=
                "<div class='border rounded p-1 col-md-3 mb-1 " +
                ((index + 1) % 3 === 0 ? "" : " mr-1") +
                "'>" +
                "<div class='text-truncate'>" +
                "<a href='attachment/" +
                file.path +
                "' download='" +
                file.name +
                "'>" +
                "<i class='" +
                getIconAttach(file.path) +
                " mr-1' aria-hidden='true'></i>" +
                file.name +
                " </a>  " +
                "</div>" +
                " </div>";
        });
        return tmpattach;
    }

    function renderComments(comments, task_id) {
        //task assigned users
        var tmp_str = "";
        comments.map(function(comment, index, arr) {
            tmp_str +=
                `<div class="media mb-1" id="comment-` +
                comment.id +
                `" ><div class="avatar bg-light-success my-0 ml-0 mr-50">
            <span class="avatar-content">` +
                comment.name.substring(0, 1) +
                `</span>
        </div>
        <div class="media-body">
            <div class="mb-0"><div class='d-flex'><div><span class="font-weight-bold">` +
                comment.name +
                `:</span><br>
            <small class="text-muted">` +
                moment(comment.created_at).format("YYYY-MM-DD hh:mm") +
                `</small></div><div>` +
                (comment.user_id == authUser.id ||
                authUser.role == "ADMIN" ||
                authUser.role == "MANAGER"
                    ? '<a id="' +
                      comment.id +
                      '" class="comment-delete waves-effect ml-1" >' +
                      "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash font-medium-1 align-middle'><polyline points='3 6 5 6 21 6'></polyline><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path></svg></a>" +
                      `</div></div> `
                    : "</div></div>") +
                comment.text +
                "<div class='row m-0 mt-1'>" +
                renderAttach(comment.attachments) +
                "</div>" +
                `</div>
            
        </div>
    </div>`;
        });
        $("#comments").html(tmp_str); //task attachments
    }

    function detailModalShow(id) {
        // Get Data for detail
        $.ajax({
            type: "GET",
            dataType: "json",
            async: false,
            url: "/task/detail/" + id,
            success: function(data) {
                detail_data = data[0];
            }
        });
        //
        if (detail_data.avatar) {
            var avatar =
                '<img class="rounded mr-50" alt="profile image" height="80" width="80" src="/avatar/' +
                detail_data.avatar +
                '"/>';
            $("#creater-profile").html(avatar);
        } else {
            var avatar =
                '<div class="bg-light-danger p-2" style="width: 80px; height: 80px; text-align: center">' +
                '<span class="font-large-1">' +
                detail_data.name.substr(0, 1) +
                "</span>" +
                "</div>";
            $("#creater-profile").html(avatar);
        }

        //task detail general info
        $("#owner").html(detail_data.lastname + " " + detail_data.name);
        $("#owner_position").html(detail_data.position);
        $("#title").html(detail_data.title);
        $("#description").html(detail_data.description);
        $("#end_date").html(moment(detail_data.end_date).format("YYYY-MM-DD"));
        $("#created_at").html(
            moment(detail_data.created_at).format("YYYY-MM-DD")
        );
        $("#this_id").val(detail_data.id);

        $("#label_new").removeClass("active");
        $("#radio_new").attr("checked", false);
        $("#label_proccesing").removeClass("active");
        $("#radio_proccesing").attr("checked", false);
        $("#label_awaiting").removeClass("active");
        $("#radio_awaiting").attr("checked", false);

        switch (detail_data.status) {
            case "NEW":
                $("#label_new").addClass("active");
                $("#radio_new").attr("checked", true);
                break;
            case "IN_PROGRESS":
                $("#label_proccesing").addClass("active");
                $("#radio_proccesing").attr("checked", true);
                break;
            case "AWAIT":
                $("#label_awaiting").addClass("active");
                $("#label_awaiting").attr("checked", true);
                break;
        }

        var onStar = detail_data.points; // The star currently selected
        for (var i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass("selected");
        }
        for (var i = 0; i < onStar; i++) {
            $(stars[i]).addClass("selected");
        }

        //task attachments
        $("#file-group").html(renderAttach(detail_data.attachments));

        $("#avatar-group").html(renderAvatar(detail_data.assigns, true, 0, 32));
        renderComments(detail_data.comments, detail_data.id);
    }

    function getIconAttach(path) {
        if (path) {
            var ext = path.split(".")[1].toLowerCase();
            switch (ext) {
                case "png":
                case "jpg":
                case "jpeg":
                    return "fa fa-picture-o";
                case "pdf":
                    return "fa fa-file-pdf-o";
                case "docx":
                    return "fa fa-file-word-o";
                case "xlsx":
                    return "fa fa-file-excel-o";
                default:
                    return "fa fa-file-archive-o";
            }
        }
    }

    /* 1. Visualizing things on Hover - See next part for action on click */
    $("#stars li")
        .on("mouseover", function() {
            var onStar = parseInt($(this).data("value"), 10); // The star currently mouse on
            // Now highlight all the stars that's not after the current hovered star
            $(this)
                .parent()
                .children("li.star")
                .each(function(e) {
                    if (e < onStar) {
                        $(this).addClass("hover");
                    } else {
                        $(this).removeClass("hover");
                    }
                });
        })
        .on("mouseout", function() {
            $(this)
                .parent()
                .children("li.star")
                .each(function(e) {
                    $(this).removeClass("hover");
                });
        });

    /* 2. Action to perform on click */
    $("#stars li").on("click", function() {
        var onStar = parseInt($(this).data("value"), 10); // The star currently selected
        var stars = $(this)
            .parent()
            .children("li.star");
        for (var i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass("selected");
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass("selected");
        }
        // JUST RESPONSE (Not needed)
        var ratingValue = parseInt(
            $("#stars li.selected")
                .last()
                .data("value"),
            10
        );
        if (
            detail_data.status == "AWAIT" &&
            (authUser.role === "ADMIN" || authUser.role === "MANAGER")
        ) {
            $.ajax({
                url: "/task/rate/" + detail_data.id,
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                data: {
                    rate: ratingValue
                },
                success: function(response) {
                    if (response) {
                        $("#kitem-" + detail_data.id).remove();
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });

    // Delete comment
    $(document).on("click", ".comment-delete", function() {
        var id = $(this)[0].id;
        if (confirm("Коммэнтийг устгах уу ?")) {
            $.ajax({
                url: "/comment/destroy/" + id,
                type: "DELETE",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                success: function(response) {
                    if (response) {
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        $("#comment-" + id).remove();
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });

    var commentForm = $(".comment-form");
    // add comment
    if (commentForm.length) {
        commentForm.validate({
            errorClass: "error",
            rules: {
                comment: {
                    required: true
                },
                task_id: {
                    required: true
                }
            }
        });

        commentForm.on("submit", function(e) {
            var isValid = commentForm.valid();
            var comment = $("#detail-item-comment").val();

            e.preventDefault();
            if (isValid) {
                var comment = $("#detail-item-comment").val();

                var uploader = $("#comment-attachment[type='file']");
                var data = new FormData();
                $.each(uploader[0].files, function() {
                    data.append("attachment[]", this);
                });
                data.append("_token", $('[name="csrf-token"]').attr("content"));
                data.append("task_id", detail_data.id);
                data.append("text", comment);

                $.ajax({
                    url: "/comment/create",
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response) {
                        if (response) {
                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });

                            $.ajax({
                                type: "GET",
                                dataType: "json",
                                async: false,
                                url: "/task/detail/" + detail_data.id,
                                //url: assetPath + 'data/kanban.json',
                                success: function(data) {
                                    detail_data = data[0];
                                }
                            });
                            renderComments(
                                detail_data.comments,
                                detail_data.id
                            );
                            //detailItemModal.find("#comments").html(tmp_str);
                            commentForm.trigger("reset");
                            document.getElementById(
                                "detail-item-comment"
                            ).value = "";
                            $(".custom-file-label").html("Файл сонгох");
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            }
        });
    }

    if (window.innerWidth <= 320) {
        $("#btn-group-status").removeClass("btn-group");
        $("#btn-group-status").removeClass("btn-group-sm");
        $("#btn-group-status").addClass("btn-group-vertical");
    }
    var id = parseInt(document.URL.split("/")[5]);

    if (!Number.isNaN(id)) {
        detailModalShow(id);
    }

    $("input[type=radio][name=status-radio]").on("change", function() {
        var status = this.value;
        $.ajax({
            url: "/task/drag",
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr("content"),
                id: detail_data.id,
                user_id: detail_data.user_id,
                status: status
            },
            success: function(response) {
                if (response) {
                    detail_data.status = status;
                    $("#kitem-" + detail_data.id).data("status", status);
                    $("#kitem-" + detail_data.id).appendTo(
                        "#board-in-" + status + "-drag"
                    );
                    toastr["success"](response.success, "Амжилттай", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            },
            error: function(response) {
                toastr["warning"](response.statusText, "Алдаа", {
                    closeButton: true,
                    tapToDismiss: false
                });
                location.reload();
            }
        });
    });
});
