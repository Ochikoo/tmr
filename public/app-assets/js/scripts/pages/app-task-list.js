/*=========================================================================================
    File Name: app-user-list.js
    Description: User List page
    --------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent

==========================================================================================*/

$(function() {
    "use strict";

    var dtUserTable = $(".archive-list-table");

    // Users List datatable
    if (dtUserTable.length) {
        var tableDta = dtUserTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: "/task2",
            columns: [
                { data: "title", name: "title" },
                { data: "priority", name: "tasks.priority" },
                { data: "name", name: "name" }
            ],
            columnDefs: [
                {
                    // For Responsive
                    className: "control",
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0
                },
                {
                    // User full name and username
                    targets: 1,
                    responsivePriority: 4,
                    render: function(data, type, full, meta) {
                        var $title = full["title"];
                        return (
                            "<span class='text-truncate align-middle'>" +
                            $title +
                            "</span>"
                        );
                    }
                },
                {
                    // User Last name
                    targets: 2,
                    render: function(data, type, full, meta) {
                        var $priority = full["priority"] || "-";
                        var badge = "";
                        var text = "";
                        switch ($priority) {
                            case "LOW":
                                badge = "badge-success";
                                text = "Энгийн";
                                break;
                            case "NORMAL":
                                badge = "badge-success";
                                text = "Дундаж";
                                break;
                            case "HIGH":
                                badge = "badge-warning";
                                text = "Яаралтай";
                                break;
                            case "HIGHEST":
                                badge = "badge-danger";
                                text = "Маш яаралтай";
                                break;
                            default:
                                badge = "badge-success";
                                text = "Энгийн";
                                break;
                        }
                        return (
                            '<span class="badge ' +
                            badge +
                            ' font-medium-1">' +
                            text +
                            "</span>"
                        );
                    }
                },
                {
                    // User Last name
                    targets: 3,
                    render: function(data, type, full, meta) {
                        if (full.avatar != null) {
                            return (
                                ' <ul class="avatar-group mb-0" style="list-style: none;">' +
                                '<li class="avatar kanban-item-avatar pull-up" data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
                                full.name +
                                '">' +
                                '<div class="avatar-container">' +
                                '<div class="avatar bg-light-primary " style="margin-right: 3px" title="' +
                                full.name +
                                '">' +
                                '<img class="round" src="avatar/' +
                                full.avatar +
                                '"                          alt="avatar"                          height="32"                          width="32"                        /> </div>' +
                                "</div></li></ul>"
                            );
                        }
                        return (
                            ' <ul class="avatar-group mb-0" style="list-style: none;">' +
                            '<li class="avatar kanban-item-avatar pull-up" data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
                            full.name +
                            '">' +
                            '<div class="avatar-container">' +
                            '<div class="avatar bg-light-primary " style="margin-right: 3px" title="' +
                            full.name +
                            '">' +
                            '<span class="avatar-content" style="font-size: 20px">' +
                            full.name.substr(0, 1) +
                            "</span></div>" +
                            "</div></li></ul>"
                        );
                    }
                },
                {
                    // User Last name
                    targets: 4,
                    render: function(data, type, full, meta) {
                        var row_output =
                            '<ul class="avatar-group mb-0" style="list-style: none;">';
                        full.assigns.forEach(element => {
                            row_output +=
                                '<li class="avatar kanban-item-avatar pull-up" data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
                                element.name +
                                '">';
                            if (element.avatar != null) {
                                row_output +=
                                    '<div class="avatar bg-light-primary " style="margin-right: 3px" title="' +
                                    element.name +
                                    '">' +
                                    '<img class="round" src="avatar/' +
                                    element.avatar +
                                    '" alt="avatar" height="32" width="32" /></div>';
                            } else {
                                row_output +=
                                    '<div class="avatar bg-light-primary " style="margin-right: 3px" title="' +
                                    element.name +
                                    '">' +
                                    '<span class="avatar-content" style="font-size: 20px">' +
                                    element.name.substr(0, 1) +
                                    "</span></div>";
                            }
                            row_output += "</li>";
                        });
                        row_output += "</ul>";
                        return row_output;
                    }
                },
                {
                    // User Last name
                    targets: 5,
                    render: function(data, type, full, meta) {
                        var id = full.id;
                        var row_output =
                            '<div class="btn-group" role="group" aria-label="Basic example">'+                          
                            '<a role="button" href="/task/view/'+id+'" class="btn btn-outline-primary"><i data-feather="maximize-2"></i></a>'+
                            '<button type="button" data-toggle="modal" data-target="#task-grid-modal2" class="btn btn-outline-primary"><i data-feather="edit"></i></button>'+
                            '<button type="button" data-toggle="modal" data-target="#task-grid-modal3" class="btn btn-outline-primary"><i data-feather="trash-2"></i></button>'+
                            '</div>';
                        return row_output;
                    }
                },
                {
                    // User Last name
                    targets: 6,
                    render: function(data, type, full, meta) {
                        var end = full.end_date;
                        return (
                            "<span class='text-truncate align-middle'>" +
                            end +
                            "</span>"
                        );
                    }
                }
            ],
            order: [[2, "desc"]],
            dom:
                '<"d-flex justify-content-between align-items-center header-actions mx-1 row mt-75"' +
                '<"col-lg-12 col-xl-6" l>' +
                '<"col-lg-12 col-xl-6 pl-xl-75 pl-0"<"dt-action-buttons text-xl-right text-lg-left text-md-right text-left d-flex align-items-center justify-content-lg-end align-items-center flex-sm-nowrap flex-wrap mr-1"<"mr-1"f>B>>' +
                ">t" +
                '<"d-flex justify-content-between mx-2 row mb-1"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                ">",
            // Buttons with Dropdown
            buttons: [],

            // For responsive popup
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function(row) {
                            var data = row.data();
                            return "Details of " + data["full_name"];
                        }
                    }),
                    type: "column",
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: "table",
                        columnDefs: [
                            {
                                targets: 2,
                                visible: false
                            },
                            {
                                targets: 3,
                                visible: false
                            }
                        ]
                    })
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: "&nbsp;",
                    next: "&nbsp;"
                }
            }
        });
    }
});
