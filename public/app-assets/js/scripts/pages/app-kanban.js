$(function() {
    "use strict";

    var boards,
        openSidebar = true,
        authUser,
        detail_data,
        kanbanWrapper = $(".kanban-wrapper"),
        newItemModal = $(".new-item-sidebar"),
        detailItemModal = $(".detail-item-sidebar"),
        commentEditor = $(".comment-editor"),
        updateItemSidebar = $(".update-item-sidebar"),
        newItemForm = $(".new-item-form"),
        commentFrom = $(".comment-form"),
        validateState = {
            errorClass: "error",
            rules: {
                title: {
                    required: true
                },
                priority: {
                    required: true
                },
                assign_id: {
                    required: true
                },
                description: {
                    required: true
                },
                end_date: {
                    required: true
                }
            }
        },
        updateItemForm = $(".update-item-form"),
        selectpicker = $(".edit-select-picker"),
        newItemForm = $(".new-item-form");

    var assetPath = "../../../app-assets/";
    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    // Get Data
    function ajax1() {
        return $.ajax({
            type: "GET",
            dataType: "json",
            async: false,
            url: "/task_get",
            success: function(data) {
                boards = data;
            }
        });
    }

    // Get User
    function ajax2() {
        return $.ajax({
            type: "GET",
            dataType: "json",
            async: false,
            url: "/user/auth",
            success: function(data) {
                authUser = data;
            }
        });
    }

    $.when(ajax1(), ajax2());

    // Comment editor
    if (commentEditor.length) {
        new Quill(".comment-editor", {
            modules: {
                toolbar: ".comment-toolbar"
            },
            placeholder: "Write a Comment... ",
            theme: "snow"
        });
    }

    // Render item dropdown
    function renderDropdown(task) {
        const cUserId = task.attr("data-user_id");
        return (
            "<div class='dropdown item-dropdown mx-1'>" +
            feather.icons["more-vertical"].toSvg({
                class: "dropdown-toggle cursor-pointer mr-0 font-medium-1",
                id: "item-dropdown",
                " data-toggle": "dropdown",
                "aria-haspopup": "true",
                "aria-expanded": "false"
            }) +
            "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='item-dropdown'>" +
            (cUserId == authUser.id || authUser.role == "MANAGER"
                ? "<a class='dropdown-item' href='javascript:void(0)' id='edit-item'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-medium-1 align-middle'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg><span class='align-middle ml-25'>Засах</span></a>"
                : "") +
            (cUserId == authUser.id
                ? "<a class='dropdown-item delete-item' href='javascript:void(0)' id='" +
                  task.attr("data-eid") +
                  "' ><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash font-medium-1 align-middle'><polyline points='3 6 5 6 21 6'></polyline><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path></svg>Устгах</a>"
                : "") +
            (task.attr("data-status") == "COMPLETED" &&
            parseInt(task.attr("data-points")) > 0
                ? "<a class='dropdown-item item-archive' href='javascript:void(0)' id='" +
                  task.attr("data-eid") +
                  "'>" +
                  feather.icons["archive"].toSvg({
                      class: "font-medium-1 align-middle"
                  }) +
                  "<span class='align-middle ml-25'>Архивлах</span></a>"
                : "") +
            "<a class='dropdown-item item-detail' href='javascript:void(0)' id='" +
            task.attr("data-eid") +
            "'>" +
            feather.icons["trello"].toSvg({
                class: "font-medium-1 align-middle"
            }) +
            "<span class='align-middle ml-25'>Дэлгэрэнгүй</span></a>" +
            "</div>" +
            "</div>"
        );
    }
    // Render header
    function renderHeader(task, text) {
        const colors = {
            LOW: "primary",
            LOWEST: "primary",
            NORMAL: "warning",
            HIGH: "danger",
            HIGHEST: "danger"
        };
        return (
            "<div class='d-flex justify-content-between flex-wrap align-items-center mb-1 pr-1'>" +
            "<div class='text-dark font-weight-bolder text-truncate'> " +
            "<span class='badge kanban-title badge-pill badge-light-" +
            colors[task.attr("data-priority")] +
            "'>" +
            text +
            "</span>" +
            "</div>" +
            renderDropdown(task) +
            "</div>"
        );
    }

    // Render avatar
    function renderAvatar(users, pullUp, margin, size) {
        var $transition = pullUp ? " pull-up" : "";
        return users !== undefined
            ? users
                  .map(function(user, index, arr) {
                      var $margin =
                          margin !== undefined && index !== arr.length - 1
                              ? " mr-" + margin + ""
                              : "";
                      var stateNum = Math.floor(Math.random() * 6) + 1;
                      var states = [
                          "success",
                          "danger",
                          "warning",
                          "info",
                          "dark",
                          "primary",
                          "secondary"
                      ];
                      var colorClass = " bg-light-" + states[stateNum] + " ";
                      if (user.avatar === undefined || user.avatar === null) {
                          return (
                              "<li class='avatar kanban-item-avatar" +
                              " " +
                              $transition +
                              " " +
                              $margin +
                              "'" +
                              "data-toggle='tooltip' data-placement='top'" +
                              "title='" +
                              user.name +
                              "'" +
                              ">" +
                              "<div class='avatar-container'><div class='avatar" +
                              colorClass +
                              "' style='margin-right: 3px' title='" +
                              user.lastName +
                              " " +
                              user.name +
                              "' >" +
                              "<span class='avatar-content' style='font-size: 20px'>" +
                              user.name.substring(0, 1) +
                              "</span></div></div></li>"
                          );
                      }
                      return (
                          "<li class='avatar kanban-item-avatar" +
                          " " +
                          $transition +
                          " " +
                          $margin +
                          "'" +
                          "data-toggle='tooltip' data-placement='top'" +
                          "title='" +
                          user.name +
                          "'" +
                          ">" +
                          "<img src='" +
                          assetPath +
                          "../avatar/" +
                          user.avatar +
                          "' alt='Avatar' height='" +
                          size +
                          "' width='" +
                          size +
                          "'>" +
                          "</li>"
                      );
                  })
                  .join(" ")
            : "";
    }

    // Render footer
    function renderFooter(attachments, comments, assigned) {
        var users = JSON.parse(assigned);
        var attach = JSON.parse(attachments).length;
        return (
            "<div class='d-flex justify-content-between align-items-center flex-wrap mt-1'>" +
            "<div> <span class='align-middle mr-50'>" +
            feather.icons["paperclip"].toSvg({
                class: "font-medium-1 align-middle mr-25"
            }) +
            "<span class='attachments align-middle'>" +
            attach +
            "</span>" +
            "</span> <span class='align-middle'>" +
            feather.icons["message-square"].toSvg({
                class: "font-medium-1 align-middle mr-25"
            }) +
            "<span>" +
            comments +
            "</span>" +
            "</span></div>" +
            "<ul class='avatar-group mb-0' style='list-style: none;'>" +
            renderAvatar(users, true, 0, 32) +
            "</ul>" +
            "</div>"
        );
    }

    function renderAttach(attachments) {
        var tmpattach = "";
        attachments.map(function(file, index, arr) {
            tmpattach +=
                "<div class='border rounded p-1 col-md-3 mb-1 " +
                ((index + 1) % 3 === 0 ? "" : " mr-1") +
                "'>" +
                "<div class='text-truncate'>" +
                "<a href='attachment/" +
                file.path +
                "' download='" +
                file.name +
                "'>" +
                "<i class='" +
                getIconAttach(file.path) +
                " mr-1' aria-hidden='true'></i>" +
                file.name +
                " </a>  " +
                "</div>" +
                " </div>";
        });
        return tmpattach;
    }

    function renderComments(comments, task_id) {
        //task assigned users
        var tmp_str = "";
        comments.map(function(comment, index, arr) {
            tmp_str +=
                `<div class="media mb-1" id="comment-` +
                comment.id +
                `" ><div class="avatar bg-light-success my-0 ml-0 mr-50">
            <span class="avatar-content">` +
                comment.name.substring(0, 1) +
                `</span>
        </div>
        <div class="media-body">
            <div class="mb-0"><div class='d-flex'><div><span class="font-weight-bold">` +
                comment.name +
                `:</span><br>
            <small class="text-muted">` +
                moment(comment.created_at).format("YYYY-MM-DD hh:mm") +
                `</small></div><div>` +
                (comment.user_id == authUser.id ||
                authUser.role == "ADMIN" ||
                authUser.role == "MANAGER"
                    ? '<a id="' +
                      comment.id +
                      '" class="comment-delete waves-effect ml-1" >' +
                      "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash font-medium-1 align-middle'><polyline points='3 6 5 6 21 6'></polyline><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path></svg></a>" +
                      `</div></div> `
                    : "</div></div>") +
                comment.text +
                "<div class='row m-0 mt-1'>" +
                renderAttach(comment.attachments) +
                "</div>" +
                `</div>
            
        </div>
    </div>`;
        });
        detailItemModal.find("#comments").html(tmp_str); //task attachments
        var tmpattach = "";
        detail_data.attachments.map(function(file, index, arr) {
            tmpattach +=
                "<div class='border rounded p-1 col-md-3 mb-1 " +
                ((index + 1) % 3 === 0 ? "" : " mr-1") +
                "'>" +
                "<div class='text-truncate'>" +
                "<a href='attachment/" +
                file.path +
                "' download='" +
                file.name +
                "'>" +
                "<i class='" +
                getIconAttach(file.path) +
                " mr-1' aria-hidden='true'></i>" +
                file.name +
                " </a>  " +
                "</div>" +
                " </div>";
        });
        detailItemModal.find("#file-group").html(tmpattach);
    }

    function detailModalShow(id) {
        detailItemModal.modal("show");
        // Get Data for detail
        $.ajax({
            type: "GET",
            dataType: "json",
            async: false,
            url: "task/detail/" + id,
            //url: assetPath + 'data/kanban.json',
            success: function(data) {
                detail_data = data[0];
            }
        });
        //
        if (detail_data.avatar) {
            var avatar =
                '<img class="rounded mr-50" alt="profile image" height="80" width="80" src="/avatar/' +
                detail_data.avatar +
                '"/>';
            detailItemModal.find("#creater-profile").html(avatar);
        } else {
            var avatar =
                '<div class="bg-light-danger p-2" style="width: 80px; height: 80px; text-align: center">' +
                '<span class="font-large-1">' +
                detail_data.name.substr(0, 1) +
                "</span>" +
                "</div>";
            detailItemModal.find("#creater-profile").html(avatar);
        }

        //task detail general info
        detailItemModal
            .find("#owner")
            .html(detail_data.lastname + " " + detail_data.name);
        detailItemModal.find("#owner_position").html(detail_data.position);
        detailItemModal.find("#title").html(detail_data.title);
        detailItemModal.find("#description").html(detail_data.description);
        detailItemModal
            .find("#end_date")
            .html(moment(detail_data.end_date).format("YYYY-MM-DD"));
        detailItemModal
            .find("#created_at")
            .html(moment(detail_data.created_at).format("YYYY-MM-DD"));
        detailItemModal.find("#this_id").val(detail_data.id);

        detailItemModal.find("#label_new").removeClass("active");
        detailItemModal.find("#radio_new").attr("checked", false);
        detailItemModal.find("#label_proccesing").removeClass("active");
        detailItemModal.find("#radio_proccesing").attr("checked", false);
        detailItemModal.find("#label_awaiting").removeClass("active");
        detailItemModal.find("#radio_awaiting").attr("checked", false);

        switch (detail_data.status) {
            case "NEW":
                detailItemModal.find("#label_new").addClass("active");
                detailItemModal.find("#radio_new").attr("checked", true);
                break;
            case "IN_PROGRESS":
                detailItemModal.find("#label_proccesing").addClass("active");
                detailItemModal.find("#radio_proccesing").attr("checked", true);
                break;
            case "AWAIT":
                detailItemModal.find("#label_awaiting").addClass("active");
                detailItemModal.find("#label_awaiting").attr("checked", true);
                break;
        }

        var onStar = detail_data.points; // The star currently selected
        var stars = $("li.star");
        for (var i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass("selected");
        }
        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass("selected");
        }

        detailItemModal
            .find("#file-group")
            .html(renderAttach(detail_data.attachments));

        detailItemModal
            .find("#avatar-group")
            .html(renderAvatar(detail_data.assigns, true, 0, 32));
        renderComments(detail_data.comments, detail_data.id);
    }

    function getIconAttach(path) {
        if (path) {
            var ext = path.split(".")[1].toLowerCase();
            switch (ext) {
                case "png":
                case "jpg":
                case "jpeg":
                    return "fa fa-picture-o";
                case "pdf":
                    return "fa fa-file-pdf-o";
                case "docx":
                    return "fa fa-file-word-o";
                case "xlsx":
                    return "fa fa-file-excel-o";
                default:
                    return "fa fa-file-archive-o";
            }
        }
    }

    // Init kanban
    var kanban = new jKanban({
        element: ".kanban-wrapper",
        gutter: "15px",
        widthBoard: "25%",
        dragItems: true,
        boards: boards,
        dragBoards: true,
        addItemButton: false,
        buttonContent: "+ Add Item",
        click: function(el, ev) {
            var el = $(el);
            var title = el.find(".kanban-title").text(),
                date = el.attr("data-end_date"),
                priority = el.attr("data-priority"),
                description = el.attr("data-description"),
                users = el.attr("data-assigns"),
                dateToUse = date.split(" ")[0];
            var assigns = JSON.parse(users).map(u => u.pivot.user_id);
            if (el.find(".kanban-item-avatar").length) {
                el.find(".kanban-item-avatar").on("click", function(e) {
                    e.stopPropagation();
                });
            }
            if (
                ev.target.id !== "item-dropdown" &&
                !$(".dropdown").hasClass("show") &&
                ev.target.tagName !== "A" &&
                openSidebar
            ) {
                detailModalShow(el.attr("data-eid"));
            }
            // KANBAN POPOVER TOGGLE
            if (
                openSidebar &&
                ev.target.tagName === "A" &&
                ev.target.id === "edit-item"
            ) {
                $("#edit-item-id").val(el.attr("data-eid"));
                $("#edit-item-title").val(title);
                $("#edit-item-priority").val(priority);
                $("#edit-item-assign_id").val(assigns);
                $("#edit-item-end-date").val(dateToUse);
                $("#edit-item-description").val(description);
                selectpicker.selectpicker("val", assigns);
                var tmp_str = "";
                JSON.parse(el.attr("data-attachments")).map(function(
                    file,
                    index,
                    arr
                ) {
                    tmp_str +=
                        "<div class='border rounded p-1 col-md-3 mb-1 " +
                        ((index + 1) % 3 === 0 ? "" : " mr-1") +
                        "' id='attach-" +
                        file.id +
                        "' > <div class='d-flex'>" +
                        "<div class='text-truncate flex-fill'>" +
                        "<i class='" +
                        getIconAttach(file.path) +
                        " mr-1' aria-hidden='true'></i>" +
                        file.name +
                        "</div>" +
                        "<a class='ml-2 attachment-delete' id='" +
                        file.id +
                        "'> <i class='fa fa-trash'></i></a>" +
                        "</div>" +
                        " </div>";
                });
                updateItemSidebar.find("#attachents-container").html(tmp_str);
                updateItemSidebar.modal("show");
            }
        },
        dragEl: function(el, source) {
            $(el)
                .find(".item-dropdown, .item-dropdown .dropdown-menu.show")
                .removeClass("show");
        },
        dropEl: function(el, t, n, i) {
            var taskID = el.dataset.eid;
            var taskStatus = el.dataset.status;
            var user = el.dataset.user_id;
            var boardStatus = t.parentNode.dataset.status;
            if (taskStatus !== boardStatus) {
                $.ajax({
                    url: "/task/drag",
                    type: "POST",
                    data: {
                        _token: $('meta[name="csrf-token"]').attr("content"),
                        id: taskID,
                        user_id: user,
                        status: boardStatus
                    },
                    success: function(response) {
                        el.dataset.status = boardStatus;
                        if (response) {
                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        location.reload();
                    }
                });
            }
        }
    });

    if (kanbanWrapper.length) {
        new PerfectScrollbar(kanbanWrapper[0]);
    }

    // Detail task
    $(document).on("click", ".dropdown-item.item-detail", function() {
        var id = $(this)[0].id;
        detailModalShow(id);
    });

    // Archive task
    $(document).on("click", ".dropdown-item.item-archive", function() {
        var id = $(this)[0].id;
        if (confirm("Таскыг архив руу шилжүүлэх үү ?")) {
            $.ajax({
                url: "/task/archive/" + id,
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                success: function(response) {
                    if (response) {
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        $("#kitem-" + id).remove();
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });

    // Open create task modal
    $(".creat-task-button").on("click", function() {
        var dateObj = new Date();
        dateObj.setDate(dateObj.getDate() + 7);
        var month = dateObj.getMonth() + 1;
        var day = dateObj.getDate();
        var dateToNew =
            dateObj.getFullYear() +
            "-" +
            (month < 10 ? "0" + month : month) +
            "-" +
            (day < 10 ? "0" + day : day);
        newItemModal
            .find("#new-end-date")
            // .next(".form-control")
            .val(dateToNew);
        newItemModal.modal("show");
    });

    // Render custom items
    $.each($(".kanban-item"), function() {
        var $this = $(this),
            $text =
                "<span class='kanban-text overflow-hidden'>" +
                ($this.attr("data-description")
                    ? $this.attr("data-description").substr(0, 100) + " ..."
                    : "-") +
                "</span>";
        if ($this.attr("data-priority") !== undefined) {
            $this.html(renderHeader($this, $this.text()) + $text);
        }
        if (
            $this.attr("data-comments") !== undefined ||
            $this.attr("data-due-date") !== undefined ||
            $this.attr("data-assigns") !== undefined
        ) {
            $this.append(
                renderFooter(
                    $this.attr("data-attachments"),
                    $this.attr("data-comments"),
                    $this.attr("data-assigns")
                )
            );
        }

        $this.on("mouseenter", function() {
            $this
                .find(".item-dropdown, .item-dropdown .dropdown-menu.show")
                .removeClass("show");
        });
    });

    // Form Validation
    if (newItemForm.length) {
        newItemForm.validate(validateState);

        newItemForm.on("submit", function(e) {
            var isValid = newItemForm.valid();
            e.preventDefault();
            if (isValid) {
                var title = $("#item-title").val();
                var priority = $("#item-priority").val();
                var assign_id = $("#item-assign_id").val();
                var description = $("#item-description").val();
                var end_date = $("#new-end-date")
                    .val()
                    .concat(" 00:00:00");
                var uploader = $("#new-attachment[type='file']");
                var data = new FormData();
                $.each(uploader[0].files, function() {
                    data.append("attachment[]", this);
                });
                data.append("_token", $('[name="csrf-token"]').attr("content"));
                data.append("title", title);
                data.append("priority", priority);
                data.append("end_date", end_date);
                data.append("assign_id", JSON.stringify(assign_id));
                data.append("description", description);
                $.ajax({
                    url: "/task/create",
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response) {
                        if (response) {
                            newItemForm.trigger("reset");
                            newItemModal.modal("hide");

                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 300);
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            }
        });
    }

    // UPDATE Form Validation
    if (updateItemForm.length) {
        updateItemForm.validate(validateState);

        updateItemForm.on("submit", function(e) {
            var isValid = updateItemForm.valid();
            e.preventDefault();
            if (isValid) {
                var taskId = $("#edit-item-id").val();
                var title = $("#edit-item-title").val();
                var priority = $("#edit-item-priority").val();
                var assign_id = $("#edit-item-assign_id").val();
                var description = $("#edit-item-description").val();
                var end_date = $("#edit-item-end-date")
                    .val()
                    .concat(" 00:00:00");

                var uploader = $("#update-attachment[type='file']");
                var data = new FormData();
                $.each(uploader[0].files, function() {
                    data.append("attachment[]", this);
                });
                data.append("_token", $('[name="csrf-token"]').attr("content"));
                data.append("title", title);
                data.append("priority", priority);
                data.append("end_date", end_date);
                data.append("assign_id", JSON.stringify(assign_id));
                data.append("description", description);
                $.ajax({
                    url: "/task/update/" + taskId,
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response) {
                        if (response) {
                            updateItemForm.trigger("reset");
                            updateItemSidebar.modal("hide");

                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });
                            setTimeout(function() {
                                location.reload();
                            }, 300);
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            }
        });
    }

    if (commentFrom.length) {
        commentFrom.validate({
            errorClass: "error",
            rules: {
                comment: {
                    required: true
                },
                task_id: {
                    required: true
                }
            }
        });

        commentFrom.on("submit", function(e) {
            var isValid = commentFrom.valid();
            var comment = $("#detail-item-comment").val();

            e.preventDefault();
            if (isValid) {
                var comment = $("#detail-item-comment").val();

                var uploader = $("#comment-attachment[type='file']");
                var data = new FormData();
                $.each(uploader[0].files, function() {
                    data.append("attachment[]", this);
                });
                data.append("_token", $('[name="csrf-token"]').attr("content"));
                data.append("task_id", detail_data.id);
                data.append("text", comment);

                $.ajax({
                    url: "/comment/create",
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(response) {
                        if (response) {
                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });

                            $.ajax({
                                type: "GET",
                                dataType: "json",
                                async: false,
                                url: "task/detail/" + detail_data.id,
                                //url: assetPath + 'data/kanban.json',
                                success: function(data) {
                                    detail_data = data[0];
                                }
                            });
                            renderComments(
                                detail_data.comments,
                                detail_data.id
                            );
                            //detailItemModal.find("#comments").html(tmp_str);
                            commentFrom.trigger("reset");
                            document.getElementById(
                                "detail-item-comment"
                            ).value = "";
                            detailItemModal
                                .find(".custom-file-label")
                                .html("Файл сонгох");
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            }
        });
    }

    // Delete comment
    $(document).on("click", ".comment-delete", function() {
        var id = $(this)[0].id;
        if (confirm("Коммэнтийг устгах уу ?")) {
            $.ajax({
                url: "/comment/destroy/" + id,
                type: "DELETE",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                success: function(response) {
                    if (response) {
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        detailItemModal.find("#comment-" + id).remove();
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });

    // Delete attachment
    $(document).on("click", ".attachment-delete", function() {
        var id = $(this)[0].id;
        if (confirm("Хавралт файлыг устгах уу ?")) {
            $.ajax({
                url: "/attachment/destroy/" + id,
                type: "DELETE",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                success: function(response) {
                    if (response) {
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        updateItemSidebar.find("#attach-" + id).remove();
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });

    // Delete task
    $(document).on("click", ".delete-item", function() {
        var id = $(this)[0].id;
        if (confirm("Таскыг устгах уу ?")) {
            $.ajax({
                url: "/task/delete/" + id,
                type: "DELETE",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                success: function(response) {
                    if (response) {
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        $("#kitem-" + id).remove();
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });
    // $("#update-attachment").on("change", function() {
    //     //this.files[0].size gets the size of your file.
    //     alert(this.files[0].size);
    // });

    /* 1. Visualizing things on Hover - See next part for action on click */
    $("#stars li")
        .on("mouseover", function() {
            var onStar = parseInt($(this).data("value"), 10); // The star currently mouse on
            // Now highlight all the stars that's not after the current hovered star
            $(this)
                .parent()
                .children("li.star")
                .each(function(e) {
                    if (e < onStar) {
                        $(this).addClass("hover");
                    } else {
                        $(this).removeClass("hover");
                    }
                });
        })
        .on("mouseout", function() {
            $(this)
                .parent()
                .children("li.star")
                .each(function(e) {
                    $(this).removeClass("hover");
                });
        });

    /* 2. Action to perform on click */
    $("#stars li").on("click", function() {
        var onStar = parseInt($(this).data("value"), 10); // The star currently selected
        var stars = $(this)
            .parent()
            .children("li.star");
        for (var i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass("selected");
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass("selected");
        }
        // JUST RESPONSE (Not needed)
        var ratingValue = parseInt(
            $("#stars li.selected")
                .last()
                .data("value"),
            10
        );
        if (
            detail_data.status == "AWAIT" &&
            (authUser.role === "ADMIN" || authUser.role === "MANAGER")
        ) {
            $.ajax({
                url: "/task/rate/" + detail_data.id,
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                data: {
                    rate: ratingValue
                },
                success: function(response) {
                    if (response) {
                        $("#kitem-" + detail_data.id).remove();
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });
    if (window.innerWidth <= 320) {
        $("#btn-group-status").removeClass("btn-group");
        $("#btn-group-status").removeClass("btn-group-sm");
        $("#btn-group-status").addClass("btn-group-vertical");
    }

    $("input[type=radio][name=status-radio]").on("change", function() {
        var status = this.value;
        $.ajax({
            url: "/task/drag",
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr("content"),
                id: detail_data.id,
                user_id: detail_data.user_id,
                status: status
            },
            success: function(response) {
                if (response) {
                    detail_data.status = status;
                    $("#kitem-" + detail_data.id).data("status", status);
                    $("#kitem-" + detail_data.id).appendTo(
                        "#board-in-" + status + "-drag"
                    );
                    toastr["success"](response.success, "Амжилттай", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            },
            error: function(response) {
                toastr["warning"](response.statusText, "Алдаа", {
                    closeButton: true,
                    tapToDismiss: false
                });
                location.reload();
            }
        });
    });

    if (updateItemSidebar.length) {
        updateItemSidebar.on("hidden.bs.modal", function() {
            updateItemSidebar.find(".custom-file-label").empty();
        });
    }
});
