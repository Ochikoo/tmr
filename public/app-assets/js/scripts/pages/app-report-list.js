/*=========================================================================================
    File Name: app-user-list.js
    Description: User List page
    --------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent

==========================================================================================*/

$(function() {
    "use strict";

    var dtUserTable = $(".report-list-table"),
        newUserSidebar = $(".new-user-modal"),
        newUserForm = $(".add-new-user");

    var pointReportModal = $("#point-report-modal"),
        pointReportForm = $(".point-report-form");

    var assetPath = "../../../app-assets/",
        userView = "#";
    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
        userView = assetPath + "app/user/view";
        userEdit = assetPath + "app/user/edit";
    }

    var authUser;
    $.ajax({
        type: "GET",
        dataType: "json",
        async: false,
        url: "/user/auth",
        success: function(data) {
            authUser = data;
        }
    });

    // Users List datatable
    if (dtUserTable.length) {
        var tableDta = dtUserTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: "/report/getAll",
            columns: [
                { data: "name", name: "name" },
                { data: "start_date", name: "start_date" },
                { data: "end_date", name: "end_date" },
                { data: "points", name: "points" }
            ],
            columnDefs: [
                {
                    // User full name and username
                    targets: 0,
                    responsivePriority: 2,
                    render: function(data, type, full, meta) {
                        var $name = full["name"],
                            $uname = full["lastName"] || "-",
                            $image = full["avatar"];

                        if ($image) {
                            // For Avatar image
                            var $output =
                                '<img src="' +
                                assetPath +
                                "../avatar/" +
                                $image +
                                '" alt="Avatar" height="32" width="32">';
                        } else {
                            // For Avatar badge
                            var stateNum = Math.floor(Math.random() * 6) + 1;
                            var states = [
                                "success",
                                "danger",
                                "warning",
                                "info",
                                "dark",
                                "primary",
                                "secondary"
                            ];
                            var $state = states[stateNum],
                                $name = full["name"],
                                $initials =
                                    $name.substr(0, 1).toUpperCase() || "";

                            $output =
                                '<span class="avatar-content">' +
                                $initials +
                                "</span>";
                        }
                        var colorClass = !$image
                            ? " bg-light-" + $state + " "
                            : "";
                        // Creates full output for row
                        var $row_output =
                            '<div class="d-flex justify-content-left align-items-center">' +
                            '<div class="avatar-wrapper">' +
                            '<div class="avatar ' +
                            colorClass +
                            ' mr-1">' +
                            $output +
                            "</div>" +
                            "</div>" +
                            '<div class="d-flex flex-column">' +
                            '<a href="' +
                            userView +
                            '" class="user_name text-truncate"><span class="font-weight-bold">' +
                            $name +
                            "</span></a>" +
                            '<small class="emp_post text-muted">' +
                            $uname +
                            "</small>" +
                            "</div>" +
                            "</div>";
                        return $row_output;
                    }
                },
                {
                    // User Last name
                    targets: 1,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $email = full["name"] || "-";
                        var $path = full["path"] || "-";
                        return (
                            "<a href='/reports/"+$path+"' download='"+$email+"'><span class='text-truncate align-middle'>" +
                            $email +
                            "</span></a>"
                        );
                    }
                },
                {
                    // User Last name
                    targets: 2,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $date = full["start_date"] || "-";
                        return (
                            "<span class='text-truncate align-middle'>" +
                            $date +
                            "</span>"
                        );
                    }
                },
                {
                    // User Position
                    targets: 3,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $end_date = full["end_date"] || "-";

                        return (
                            "<span class='text-truncate align-middle'>" +
                            $end_date +
                            "</span>"
                        );
                    }
                },
                {
                    // User Depart
                    targets: 4,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $viewer = full.creater;
                        if ($viewer.avatar != null) {
                            return (
                                ' <ul class="avatar-group mb-0" style="list-style: none;">' +
                                '<li class="avatar kanban-item-avatar pull-up" data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
                                $viewer.name +
                                '">' +
                                '<div class="avatar-container">' +
                                '<div class="avatar bg-light-primary " style="margin-right: 3px" title="' +
                                $viewer.name +
                                '">' +
                                '<img class="round" src="avatar/' +
                                $viewer.avatar +
                                '"                          alt="avatar"                          height="32"                          width="32"                        /> </div>' +
                                "</div></li></ul>"
                            );
                        }
                        return (
                            ' <ul class="avatar-group mb-0" style="list-style: none;">' +
                            '<li class="avatar kanban-item-avatar pull-up" data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
                            $viewer.name +
                            '">' +
                            '<div class="avatar-container">' +
                            '<div class="avatar bg-light-primary " style="margin-right: 3px" title="' +
                            $viewer.name +
                            '">' +
                            '<span class="avatar-content" style="font-size: 20px">' +
                            $viewer.name.substr(0, 1) +
                            "</span></div>" +
                            "</div></li></ul>"
                        );
                    }
                },
                {
                    // User Role
                    targets: 5,
                    render: function(data, type, full, meta) {
                        var points = full.points;
                        var row_output =
                            "<div class='rating-stars d-flex'>" +
                            "<ul id='stars'>" +
                            "<li class='star" +
                            (points >= 1 ? " selected" : "") +
                            "' title='Муу' data-value='1'> <i class='fa fa-star fa-fw'></i> </li>" +
                            "<li class='star" +
                            (points >= 2 ? " selected" : "") +
                            "' title='Хангалтгүй' data-value='2'><i class='fa fa-star fa-fw'></i></li>" +
                            "<li class='star" +
                            (points >= 3 ? " selected" : "") +
                            "' title='Дундаж' data-value='3'><i class='fa fa-star fa-fw'></i></li>" +
                            "<li class='star" +
                            (points >= 4 ? " selected" : "") +
                            "' title='Сайн' data-value='4'><i class='fa fa-star fa-fw'></i></li>" +
                            "<li class='star" +
                            (points >= 5 ? " selected" : "") +
                            "' title='Маш сайн' data-value='5'><i class='fa fa-star fa-fw'></i></li>" +
                            "</ul>" +
                            "<div class='text-dark ml-1' style='padding-top: 2px;'> " +
                            points +
                            " / 5" +
                            "</div></div>";
                        return row_output;
                    }
                },
                {
                    // User Depart
                    targets: 6,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $viewer = full.viewer;
                        if ($viewer.avatar != null) {
                            return (
                                ' <ul class="avatar-group mb-0" style="list-style: none;">' +
                                '<li class="avatar kanban-item-avatar pull-up" data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
                                $viewer.name +
                                '">' +
                                '<div class="avatar-container">' +
                                '<div class="avatar bg-light-primary " style="margin-right: 3px" title="' +
                                $viewer.name +
                                '">' +
                                '<img class="round" src="avatar/' +
                                $viewer.avatar +
                                '"                          alt="avatar"                          height="32"                          width="32"                        /> </div>' +
                                "</div></li></ul>"
                            );
                        }
                        return (
                            ' <ul class="avatar-group mb-0" style="list-style: none;">' +
                            '<li class="avatar kanban-item-avatar pull-up" data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
                            $viewer.name +
                            '">' +
                            '<div class="avatar-container">' +
                            '<div class="avatar bg-light-primary " style="margin-right: 3px" title="' +
                            $viewer.name +
                            '">' +
                            '<span class="avatar-content" style="font-size: 20px">' +
                            $viewer.name.substr(0, 1) +
                            "</span></div>" +
                            "</div></li></ul>"
                        );
                    }
                },
                {
                    // Actions
                    targets: 7,
                    responsivePriority: 1,
                    title: "Actions",
                    orderable: false,
                    render: function(data, type, full, meta) {
                        // data-toggle="modal" data-target="#point-report-modal"
                        return (
                            '<div class="d-flex">' +
                            (authUser.id === full.viewer_id
                                ? '<button type="button" class="btn btn-icon rounded-circle btn-outline-primary waves-effect point-report ml-1"  id="' +
                                  full.id +
                                  '" >' +
                                  '<i class="fa fa-pencil"> </i>' +
                                  "</button>"
                                : "") +
                            (authUser.id === full.creater_id
                                ? '<button type="button" class="btn btn-icon rounded-circle btn-outline-primary waves-effect delete-user ml-1" id="' +
                                  full.id +
                                  '" >' +
                                  '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline>' +
                                  '<path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>' +
                                  "</button>"
                                : "") +
                            "</div>"
                        );
                    }
                }
            ],
            order: [[2, "desc"]],
            dom:
                '<"d-flex justify-content-between align-items-center header-actions mx-1 row mt-75"' +
                '<"col-lg-12 col-xl-6" l>' +
                '<"col-lg-12 col-xl-6 pl-xl-75 pl-0"<"dt-action-buttons text-xl-right text-lg-left text-md-right text-left d-flex align-items-center justify-content-lg-end align-items-center flex-sm-nowrap flex-wrap mr-1"<"mr-1"f>B>>' +
                ">t" +
                '<"d-flex justify-content-between mx-2 row mb-1"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                ">",
            // Buttons with Dropdown
            buttons: [
                {
                    text: "Тайлан нэмэх",
                    className:
                        "add-new btn btn-outline-primary mt-50 waves-effect",
                    attr: {
                        "data-toggle": "modal",
                        "data-target": "#modals-slide-in"
                    },
                    init: function(api, node, config) {
                        $(node).removeClass("btn-secondary");
                    }
                }
            ],
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: "&nbsp;",
                    next: "&nbsp;"
                }
            }
        });
    }

    // Form Validation
    if (newUserForm.length) {
        newUserForm.validate({
            errorClass: "error",
            rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                viewer: {
                    required: true
                },
                attachment: {
                    required: true
                }
            }
        });

        newUserForm.on("submit", function(e) {
            var isValid = newUserForm.valid();

            if (isValid) {
                var start_date = $("#start-date").val();
                var end_date = $("#end-date").val();
                var viewer = $("#viewer").val();
                var attachment = $("#attachment[type='file']");
                var data = new FormData();
                $.each(attachment[0].files, function() {
                    data.append("attachment", this);
                });

                data.append("start_date", start_date);
                data.append(
                    "_token",
                    $('meta[name="csrf-token"]').attr("content")
                );

                data.append("end_date", end_date);
                data.append("viewer_id", viewer);

                $.ajax({
                    url: "/report/create",

                    type: "POST",
                    processData: false,
                    contentType: false,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    data: data,
                    success: function(response) {
                        if (response) {
                            newUserForm.trigger("reset");
                            newUserSidebar.find(".custom-file-label").empty();
                            newUserSidebar
                                .find("#avatarPreview")
                                .attr("src", "avatar/avatarDefault.png");
                            newUserSidebar.modal("hide");
                            tableDta.ajax.reload();
                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        newUserForm.trigger("reset");
                        newUserSidebar.find(".custom-file-label").empty();
                        newUserSidebar
                            .find("#avatarPreview")
                            .attr("src", "avatar/avatarDefault.png");
                    }
                });
            } else {
                console.log("xaxa");
            }
            e.preventDefault();
        });
    }

    $(document).on("click", ".delete-user", function() {
        var id = $(this)[0].id;
        if (confirm("Тайлан устгахдаа итгэлтэй байна уу ?")) {
            $.ajax({
                url: "/report/delete/" + id,
                type: "DELETE",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                success: function(response) {
                    if (response) {
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        tableDta.ajax.reload();
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });

    tableDta.on("click", ".point-report", function() {
        var data = tableDta.row($(this).closest("tr")).data();
        pointReportForm.trigger("reset");
        var $viewer = data.creater;

        $("#report-id").val(data.id);
        var tmp_str = "";

        if ($viewer.avatar) {
            tmp_str =
                '<img class="rounded mr-50" alt="profile image" height="80" width="80" src="/avatar/' +
                $viewer.avatar +
                '"/>';
        } else {
            tmp_str =
                '<div class="bg-light-danger p-2" style="width: 80px; height: 80px; text-align: center">' +
                '<span class="font-large-1">' +
                $viewer.name.substr(0, 1) +
                "</span>" +
                "</div>";
        }

        pointReportModal.find("#creater-profile").html(tmp_str);
        pointReportModal
            .find("#creater")
            .html($viewer.lastName + " " + $viewer.name);
        pointReportModal.find("#report-name").html("<a href='"+data.path+"' download='"+data.name+"'>"+data.name+"</a>");

        var onStar = data.points; // The star currently selected
        var stars = pointReportModal.find("li.star");
        for (var i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass("selected");
        }
        for (var i = 0; i < onStar; i++) {
            $(stars[i]).addClass("selected");
        }
        pointReportModal.modal("show");

        // $("#user-password").val(data.password);
    });

    /* 1. Visualizing things on Hover - See next part for action on click */
    $("#stars li")
        .on("mouseover", function() {
            var onStar = parseInt($(this).data("value"), 10); // The star currently mouse on
            // Now highlight all the stars that's not after the current hovered star
            $(this)
                .parent()
                .children("li.star")
                .each(function(e) {
                    if (e < onStar) {
                        $(this).addClass("hover");
                    } else {
                        $(this).removeClass("hover");
                    }
                });
        })
        .on("mouseout", function() {
            $(this)
                .parent()
                .children("li.star")
                .each(function(e) {
                    $(this).removeClass("hover");
                });
        });
    /* 2. Action to perform on click */
    $("#stars li").on("click", function() {
        var onStar = parseInt($(this).data("value"), 10); // The star currently selected
        var stars = $(this)
            .parent()
            .children("li.star");
        for (var i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass("selected");
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass("selected");
        }
        // JUST RESPONSE (Not needed)
        var ratingValue = parseInt(
            $("#stars li.selected")
                .last()
                .data("value"),
            10
        );
        var reportId = $("#report-id").val();

        $.ajax({
            url: "/report/rate/" + reportId,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            data: { rate: parseInt(ratingValue, 10) },
            success: function(response) {
                if (response) {
                    pointReportForm.trigger("reset");
                    newUserSidebar.modal("hide");
                    tableDta.ajax.reload();
                    toastr["success"](response.success, "Амжилттай", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            },
            error: function(response) {
                toastr["warning"](response.statusText, "Алдаа", {
                    closeButton: true,
                    tapToDismiss: false
                });
                pointReportForm.trigger("reset");
            }
        });
    });
});
