/*=========================================================================================
    File Name: app-user-list.js
    Description: User List page
    --------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent

==========================================================================================*/

$(function() {
    "use strict";

    var dtUserTable = $(".user-list-table"),
        newUserSidebar = $(".new-user-modal"),
        passwordUserSidebar = $(".password-user-modal"),
        newUserForm = $(".add-new-user");

    var editUserSidebar = $(".edit-user-modal"),
        passwordUserForm = $(".password-user-form"),
        editUserForm = $(".edit-user-form");

    var assetPath = "../../../app-assets/",
        userView = "#";
    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
        userView = assetPath + "app/user/view";
        userEdit = assetPath + "app/user/edit";
    }

    var authUser;
    $.ajax({
        type: "GET",
        dataType: "json",
        async: false,
        url: "/user/auth",
        success: function(data) {
            authUser = data;
        }
    });

    // Users List datatable
    if (dtUserTable.length) {
        var tableDta = dtUserTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: "/user/getAll",
            columns: [
                { data: "name", name: "users.name" },
                { data: "email", name: "users.email" },
                { data: "phone", name: "users.phone" },
                { data: "position", name: "users.position" },
                { data: "role", name: "users.role" },
                { data: "departments.name", name: "department.name" }
            ],
            columnDefs: [
                {
                    // User full name and username
                    targets: 0,
                    responsivePriority: 2,
                    render: function(data, type, full, meta) {
                        var $name = full["name"],
                            $uname = full["lastName"] || "-",
                            $image = full["avatar"];

                        if ($image) {
                            // For Avatar image
                            var $output =
                                '<img src="' +
                                assetPath +
                                "../avatar/" +
                                $image +
                                '" alt="Avatar" height="32" width="32">';
                        } else {
                            // For Avatar badge
                            var stateNum = Math.floor(Math.random() * 6) + 1;
                            var states = [
                                "success",
                                "danger",
                                "warning",
                                "info",
                                "dark",
                                "primary",
                                "secondary"
                            ];
                            var $state = states[stateNum],
                                $name = full["name"],
                                $initials =
                                    $name.substr(0, 1).toUpperCase() || "";

                            $output =
                                '<span class="avatar-content">' +
                                $initials +
                                "</span>";
                        }
                        var colorClass = !$image
                            ? " bg-light-" + $state + " "
                            : "";
                        // Creates full output for row
                        var $row_output =
                            '<div class="d-flex justify-content-left align-items-center">' +
                            '<div class="avatar-wrapper">' +
                            '<div class="avatar ' +
                            colorClass +
                            ' mr-1">' +
                            $output +
                            "</div>" +
                            "</div>" +
                            '<div class="d-flex flex-column">' +
                            '<a href="' +
                            userView +
                            '" class="user_name text-truncate"><span class="font-weight-bold">' +
                            $name +
                            "</span></a>" +
                            '<small class="emp_post text-muted">' +
                            $uname +
                            "</small>" +
                            "</div>" +
                            "</div>";
                        return $row_output;
                    }
                },
                {
                    // User Last name
                    targets: 1,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $email = full["email"] || "-";
                        return (
                            "<span class='text-truncate align-middle'>" +
                            $email +
                            "</span>"
                        );
                    }
                },
                {
                    // User Last name
                    targets: 2,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $email = full["phone"] || "-";
                        return (
                            "<span class='text-truncate align-middle'>" +
                            $email +
                            "</span>"
                        );
                    }
                },
                {
                    // User Position
                    targets: 3,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $position = full["position"] || "-";

                        return (
                            "<span class='text-truncate align-middle'>" +
                            $position +
                            "</span>"
                        );
                    }
                },
                {
                    // User Role
                    targets: 4,
                    visible:
                        authUser && authUser.role && authUser.role === "ADMIN"
                            ? true
                            : false,
                    render: function(data, type, full, meta) {
                        var $role = full["role"] || "-";

                        var $roles = {
                            ADMIN: "Админ",
                            MANAGER: "Дарга",
                            STAFF: "Ажилчин"
                        };

                        return (
                            "<span class='text-truncate align-middle badge badge-primary'>" +
                            $roles[$role] +
                            "</span>"
                        );
                    }
                },
                {
                    // User Depart
                    targets: 5,
                    responsivePriority: 1,
                    render: function(data, type, full, meta) {
                        var $depart = full.department.name || "-";

                        return (
                            "<span class='text-truncate align-middle'>" +
                            $depart +
                            "</span>"
                        );
                    }
                },
                {
                    // Actions
                    targets: 6,
                    responsivePriority: 1,
                    title: "Actions",
                    orderable: false,
                    visible:
                        authUser && authUser.role && authUser.role === "ADMIN"
                            ? true
                            : false,
                    render: function(data, type, full, meta) {
                        return (
                            '<div class="d-flex">' +
                            '<button type="button" data-toggle="modal" data-target="#modals-edit-user" class="btn btn-icon rounded-circle btn-outline-primary waves-effect edit">' +
                            '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2">' +
                            '<path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>' +
                            "</button>" +
                            '<button type="button" class="btn btn-icon rounded-circle btn-outline-primary waves-effect delete-user ml-1" id="' +
                            full.id +
                            '" >' +
                            '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline>' +
                            '<path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>' +
                            "</button>" +
                            '<button type="button" data-toggle="modal" data-target="#modals-password-user" class="btn btn-icon rounded-circle btn-outline-primary waves-effect password-user ml-1" >' +
                            '<i class="fa fa-key" />' +
                            "</button>" +
                            "</div>"
                        );
                    }
                }
            ],
            order: [[2, "desc"]],
            dom:
                '<"d-flex justify-content-between align-items-center header-actions mx-1 row mt-75"' +
                '<"col-lg-12 col-xl-6" l>' +
                '<"col-lg-12 col-xl-6 pl-xl-75 pl-0"<"dt-action-buttons text-xl-right text-lg-left text-md-right text-left d-flex align-items-center justify-content-lg-end align-items-center flex-sm-nowrap flex-wrap mr-1"<"mr-1"f>B>>' +
                ">t" +
                '<"d-flex justify-content-between mx-2 row mb-1"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                ">",
            // Buttons with Dropdown
            buttons:
                authUser.role === "ADMIN"
                    ? [
                          {
                              text: "Хэрэглэгч нэмэх",
                              className:
                                  "add-new btn btn-outline-primary mt-50 waves-effect",
                              attr: {
                                  "data-toggle": "modal",
                                  "data-target": "#modals-slide-in"
                              },
                              init: function(api, node, config) {
                                  $(node).removeClass("btn-secondary");
                              }
                          }
                      ]
                    : [],

            // For responsive popup
            // responsive: {
            //     details: {
            //         display: $.fn.dataTable.Responsive.display.modal({
            //             header: function(row) {
            //                 var data = row.data();
            //                 return data["name"]+" дэлгэрэнгүй";
            //             }
            //         }),
            //         type: "column",
            //         renderer: $.fn.dataTable.Responsive.renderer.tableAll({
            //             tableClass: "table",
            //             columnDefs: [
            //                 {"targets": [7] , "visible":false}
            //             ]
            //         })
            //     }
            // },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: "&nbsp;",
                    next: "&nbsp;"
                }
            }
        });
    }

    // Table actions onclick
    tableDta.on("click", ".edit", function() {
        var data = tableDta.row($(this).closest("tr")).data();
        editUserForm.trigger("reset");
        $("#user-edit-id").val(data.id);
        $("#user-edit-name").val(data.name);
        $("#user-edit-lastname").val(data.lastName);
        $("#user-edit-email").val(data.email);
        $("#user-edit-department-id").val(data.department_id);
        $("#user-edit-role").val(data.role);
        $("#user-edit-token").val(data.token);
        $("#user-edit-phone").val(data.phone);
        $("#user-edit-position").val(data.position);
        // $("#user-password").val(data.password);
    });

    // Table actions onclick
    tableDta.on("click", ".password-user", function() {
        var data = tableDta.row($(this).closest("tr")).data();
        passwordUserForm.trigger("reset");
        $("#user-pwd-id").val(data.id);
        if (data.avatar) {
            var avatar =
                '<img class="rounded mr-50" alt="profile image" height="80" width="80" src="/avatar/' +
                data.avatar +
                '"/>';
            passwordUserSidebar.find("#user-profile").html(avatar);
        } else {
            var avatar =
                '<div class="bg-light-danger p-2" style="width: 80px; height: 80px; text-align: center">' +
                '<span class="font-large-1">' +
                data.name.substr(0, 1) +
                "</span>" +
                "</div>";
            passwordUserSidebar.find("#user-profile").html(avatar);
        }
        var str =
            "<div>" +
            data.lastName +
            " " +
            data.name +
            "</div><div>" +
            data.email +
            "</div>";
        passwordUserSidebar.find("#user-name").html(str);
        // $("#user-password").val(data.password);
    });

    // Form Validation
    if (editUserForm.length) {
        editUserForm.validate({
            errorClass: "error",
            rules: {
                name: {
                    required: true
                },
                lastName: {
                    required: true
                },
                email: {
                    required: true
                },
                role: {
                    required: true
                },
                department_id: {
                    required: true
                },
                phone: {
                    maxlength: 8,
                    minlength: 8,
                    number: true
                }
            }
        });

        editUserForm.on("submit", function(e) {
            var isValid = editUserForm.valid();
            e.preventDefault();
            if (isValid) {
                var name = $("#user-edit-name").val();
                var lastName = $("#user-edit-lastname").val();
                var email = $("#user-edit-email").val();
                var department_id = $("#user-edit-department-id").val();
                var role = $("#user-edit-role").val();
                var phone = $("#user-edit-phone").val();
                var token = $("#user-edit-token").val();
                var position = $("#user-edit-position").val();
                var id = $("#user-edit-id").val();

                $.ajax({
                    url: "/user/update/" + id,
                    type: "PUT",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    data: {
                        _token: $('meta[name="csrf-token"]').attr("content"),
                        name: name,
                        department_id: department_id,
                        email: email,
                        lastName: lastName,
                        role: role,
                        phone: phone,
                        token: token,
                        position: position
                    },
                    success: function(response) {
                        if (response) {
                            editUserForm.trigger("reset");
                            editUserSidebar.modal("hide");
                            tableDta.ajax.reload();
                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            }
        });
    }

    // Form Validation
    if (newUserForm.length) {
        newUserForm.validate({
            errorClass: "error",
            rules: {
                name: {
                    required: true
                },
                lastName: {
                    required: true
                },
                email: {
                    required: true
                },
                role: {
                    required: true
                },
                department_id: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 8
                },
                "password-confirmation": {
                    minlength: 8,
                    equalTo: "#user-password"
                },
                phone: {
                    maxlength: 8,
                    minlength: 8,
                    number: true
                }
            }
        });

        newUserForm.on("submit", function(e) {
            var isValid = newUserForm.valid();
            e.preventDefault();
            if (isValid) {
                var name = $("#user-name").val();
                var lastName = $("#user-lastname").val();
                var email = $("#user-email").val();
                var avatar = $("#user-avatar[type='file']");
                var data = new FormData();
                $.each(avatar[0].files, function() {
                    data.append("avatar", this);
                });
                var department_id = $("#user-department-id").val();
                var role = $("#user-role").val();
                var phone = $("#user-phone").val();
                var token = $("#user-token").val();
                var position = $("#user-position").val();
                var password = $("#user-password").val();
                data.append("name", name);
                data.append(
                    "_token",
                    $('meta[name="csrf-token"]').attr("content")
                );

                data.append("department_id", department_id);
                data.append("email", email);
                data.append("lastName", lastName);
                data.append("role", role);
                data.append("phone", phone);
                data.append("position", position);
                data.append("rtoken", token);
                data.append("password", password);

                $.ajax({
                    url: "/user/create",
                    type: "POST",
                    processData: false,
                    contentType: false,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    data: data,
                    success: function(response) {
                        if (response) {
                            newUserForm.trigger("reset");
                            newUserSidebar.find(".custom-file-label").empty();
                            newUserSidebar
                                .find("#avatarPreview")
                                .attr("src", "avatar/avatarDefault.png");
                            newUserSidebar.modal("hide");
                            tableDta.ajax.reload();
                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        newUserForm.trigger("reset");
                        newUserSidebar.find(".custom-file-label").empty();
                        newUserSidebar
                            .find("#avatarPreview")
                            .attr("src", "avatar/avatarDefault.png");
                    }
                });
            }
        });
    }

    // Form Validation
    if (passwordUserForm.length) {
        passwordUserForm.validate({
            errorClass: "error",
            rules: {
                password: {
                    required: true,
                    minlength: 8
                },
                confirm_password: {
                    minlength: 8,
                    equalTo: "#password"
                }
            }
        });

        passwordUserForm.on("submit", function(e) {
            var isValid = passwordUserForm.valid();
            e.preventDefault();
            if (isValid) {
                var data = new FormData();
                var password = $("#password").val();
                var id = $("#user-pwd-id").val();
                data.append(
                    "_token",
                    $('meta[name="csrf-token"]').attr("content")
                );
                data.append("password", password);
                data.append("id", id);
                $.ajax({
                    url: "/user/password",
                    type: "POST",
                    processData: false,
                    contentType: false,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    data: data,
                    success: function(response) {
                        if (response) {
                            passwordUserForm.trigger("reset");
                            passwordUserSidebar.modal("hide");
                            tableDta.ajax.reload();
                            toastr["success"](response.success, "Амжилттай", {
                                closeButton: true,
                                tapToDismiss: false
                            });
                        }
                    },
                    error: function(response) {
                        toastr["warning"](response.statusText, "Алдаа", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        passwordUserForm.trigger("reset");
                    }
                });
            }
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $("#avatarPreview").attr("src", e.target.result);
            };

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $("#user-avatar").on("change", function() {
        readURL(this);
    });

    $(document).on("click", ".delete-user", function() {
        var id = $(this)[0].id;
        if (confirm("Хэрэглэгчийг устгахдаа итгэлтэй байна уу ?")) {
            $.ajax({
                url: "/user/delete/" + id,
                type: "DELETE",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                success: function(response) {
                    if (response) {
                        toastr["success"](response.success, "Амжилттай", {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        tableDta.ajax.reload();
                    }
                },
                error: function(response) {
                    toastr["warning"](response.statusText, "Алдаа", {
                        closeButton: true,
                        tapToDismiss: false
                    });
                }
            });
        }
    });
});
